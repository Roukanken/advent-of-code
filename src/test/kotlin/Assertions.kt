import strikt.api.Assertion
import strikt.assertions.elementAt
import strikt.assertions.isEqualTo

fun <T : Sequence<E>, E> Assertion.Builder<T>.startsWith(vararg values: E) =
    get("at start contains exactly in order") { take(values.size).toList() }.and {
        values.forEachIndexed { index, value ->
            elementAt(index) isEqualTo value
        }
    }
