package y2020

import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.isEqualTo

internal class Puzzle2Test {
    private fun getSolver() = Puzzle2("/2020/day2.input")

    @Test
    internal fun `policy parses`() {
        val policy = Policy.parse("42-47 a")

        expectThat(policy) {
            get { character } isEqualTo 'a'
            get { range } isEqualTo 42..47
        }
    }

    @Test
    internal fun `entry parses`() {
        val entry = Entry.parse("42-47 a: pass")

        expectThat(entry) {
            get { policy }.and {
                get { character } isEqualTo 'a'
                get { range } isEqualTo 42..47
            }

            get { password } isEqualTo Password("pass")
        }
    }

    @Test
    internal fun `policy validates password`() {
        val policy = Policy(1..2, 'a')

        expectThat(policy) {
            get { policy.isValidOld(Password("abra")) } isEqualTo true
            get { policy.isValidOld(Password("kadabra")) } isEqualTo false
            get { policy.isValidOld(Password("fluffy")) } isEqualTo false
        }
    }

    @Test
    internal fun `policy validates password by new way`() {
        val policy = Policy(1..2, 'a')

        expectThat(policy) {
            get { policy.isValidNew(Password("abaaa")) } isEqualTo true
            get { policy.isValidNew(Password("ddaaa")) } isEqualTo false
            get { policy.isValidNew(Password("aaaaa")) } isEqualTo false
        }
    }


    @Test
    fun `solves first part`() {
        val result = getSolver().solveFirstPart()

        expectThat(result) isEqualTo 556
    }

    @Test
    fun `solves second part`() {
        val result = getSolver().solveSecondPart()

        expectThat(result) isEqualTo 605
    }
}