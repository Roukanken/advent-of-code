package y2020

import at
import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.isEqualTo

internal class Puzzle11Test {

    private fun getSolver() = Puzzle11("/2020/day11.input")

    @Test
    fun `automata parses`() {
        val plan = """
            #.
            L#
        """.trimIndent()

        val automata = CellAutomata.parse(plan, NeighborhoodsRule, Seat::parse)

        expectThat(automata) {
            get { this[0 at 0] } isEqualTo Seat.FULL
            get { this[1 at 0] } isEqualTo Seat.FLOOR
            get { this[0 at 1] } isEqualTo Seat.EMPTY
        }
    }

    @Test
    fun `automata stabilizes`() {
        val plan = """
            ..
            ..
        """.trimIndent()

        val automata = CellAutomata.parse(plan, NeighborhoodsRule, Seat::parse)

        expectThat(automata).isEqualTo(automata.next())
    }


    @Test
    fun `automata updates`() {
        val plan = """
            L.LL
            LLLL
            L###
            L###
        """.trimIndent()

        val updated = CellAutomata.parse(plan, NeighborhoodsRule, Seat::parse).next()

        expectThat(updated) {
            get { this[0 at 0] } isEqualTo Seat.FULL
            get { this[2 at 3] } isEqualTo Seat.EMPTY
        }
    }


    @Test
    fun `automata updates harder`() {
        val plan = """
            #.LL.L#.##
            #LLLLLL.L#
            L.L.L..L..
            #LLL.LL.L#
            #.LL.LL.LL
            #.LLLL#.##
            ..L.L.....
            #LLLLLLLL#
            #.LLLLLL.L
            #.#LLLL.##
        """.trimIndent()

        val updated = CellAutomata.parse(plan, NeighborhoodsRule, Seat::parse)

        expectThat(updated) {
            get { this[1 at 0] } isEqualTo Seat.FLOOR
            get { next().toReadable() }
                .isEqualTo(
                    """
                #.##.L#.##
                #L###LL.L#
                L.#.#..#..
                #L##.##.L#
                #.##.LL.LL
                #.###L#.##
                ..#.#.....
                #L######L#
                #.LL###L.L
                #.#L###.##
            """.trimIndent()
                )
        }
    }

    @Test
    fun `automata ray updates harder`() {
        val plan = """
            #.LL.LL.L#
            #LLLLLL.LL
            L.L.L..L..
            LLLL.LL.LL
            L.LL.LL.LL
            L.LLLLL.LL
            ..L.L.....
            LLLLLLLLL#
            #.LLLLLL.L
            #.LLLLL.L#
        """.trimIndent()

        val updated = CellAutomata.parse(plan, VisibilityRule, Seat::parse)

        expectThat(updated) {
            get { next().toReadable() }
                .isEqualTo(
                    """
                #.L#.##.L#
                #L#####.LL
                L.#.#..#..
                ##L#.##.##
                #.##.#L.##
                #.#####.#L
                ..#.#.....
                LLL####LL#
                #.L#####.L
                #.L####.L#
            """.trimIndent()
                )
        }
    }

    @Test
    fun `solves first part`() {
        val result = getSolver().solveFirstPart()

        expectThat(result) isEqualTo 2476
    }

    @Test
    fun `solves second part`() {
        val result = getSolver().solveSecondPart()

        expectThat(result) isEqualTo 2257
    }
}
