package y2020

import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.isEqualTo

internal class Puzzle24Test {

    private fun getSolver() = Puzzle24("/2020/day24.input")

    @Test
    fun `solves first part`() {
        val result = getSolver().solveFirstPart()

        expectThat(result) isEqualTo 485
    }

    @Test
    fun `solves second part`() {
        val result = getSolver().solveSecondPart()

        expectThat(result) isEqualTo 3933
    }
}
