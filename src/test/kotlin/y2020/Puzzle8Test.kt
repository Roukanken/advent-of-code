package y2020

import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.isA
import strikt.assertions.isEqualTo
import y2020.Instruction.*
import startsWith

internal class Puzzle8Test {

    private fun getSolver() = Puzzle8("/2020/day8.input")

    @Test
    fun `NoOperation parses`() {
        val result = Instruction.parse("nop +42")

        expectThat(result)
            .isA<NoOperation>()
    }

    @Test
    fun `Accumulate parses`() {
        val result = Instruction.parse("acc +42")

        expectThat(result)
            .isA<Accumulate>()
            .get { value } isEqualTo 42
    }

    @Test
    fun `Jump parses`() {
        val result = Instruction.parse("jmp -42")

        expectThat(result)
            .isA<Jump>()
            .get { value } isEqualTo -42
    }

    @Test
    fun `Accumulate increases console`() {
        val instruction = Accumulate(42)

        val result = instruction.execute(Console(1, 5))

        expectThat(result) {
            get { accumulator } isEqualTo 43
            get { instructionPointer } isEqualTo 6
        }
    }

    @Test
    fun `NoOperation does nothing`() {
        val instruction = NoOperation(42)

        val result = instruction.execute(Console(1, 5))

        expectThat(result) {
            get { accumulator } isEqualTo 1
            get { instructionPointer } isEqualTo 6
        }
    }

    @Test
    fun `Jump messes with pointer`() {
        val instruction = Jump(42)

        val result = instruction.execute(Console(1, 5))

        expectThat(result) {
            get { accumulator } isEqualTo 1
            get { instructionPointer } isEqualTo 47
        }
    }

    @Test
    fun `Console execute works correctly on sample`() {
        val instructions = listOf(
            NoOperation(0),
            Accumulate(1),
            Jump(4),
            Accumulate(3),
            Jump(-3),
            Accumulate(-99),
            Accumulate(1),
            Jump(-4),
            Accumulate(6),
        )

        val sequence = Console.execute(instructions)

        expectThat(sequence)
            .startsWith(
                Console(0, 0),
                Console(0, 1),
                Console(1, 2),
                Console(1, 6),
                Console(2, 7),
                Console(2, 3),
                Console(5, 4),
                Console(5, 1),
            )
    }

    @Test
    fun `solves first part`() {
        val result = getSolver().solveFirstPart()

        expectThat(result) isEqualTo 2025
    }

    @Test
    fun `solves second part`() {
        val result = getSolver().solveSecondPart()

        expectThat(result) isEqualTo 2001
    }
}

