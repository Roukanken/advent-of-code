package y2020

import org.junit.jupiter.api.Test
import strikt.api.Assertion
import strikt.api.expectThat
import strikt.assertions.elementAt
import strikt.assertions.isA
import strikt.assertions.isEqualTo
import y2020.Instruction.*

internal class Puzzle9Test {

    private fun getSolver() = Puzzle9("/2020/day9.input")

    @Test
    fun `solves first part`() {
        val result = getSolver().solveFirstPart()

        expectThat(result) isEqualTo 375054920
    }

    @Test
    fun `solves second part`() {
        val result = getSolver().solveSecondPart()

        expectThat(result) isEqualTo 54142584
    }
}
