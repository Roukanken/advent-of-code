package y2020

import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.isEqualTo
import strikt.assertions.isNotNull

internal class Puzzle1Test {

    private val solver = Puzzle1("/2020/day1.input")

    @Test
    fun `finds correct pair`() {
        val pair = solver.findPairWithSum(2020)

        expectThat(pair)
            .isNotNull()
            .get { first + second } isEqualTo 2020
    }

    @Test
    fun `solves first part`() {
        val result = solver.solveFirstPart()

        expectThat(result) isEqualTo 751776
    }

    @Test
    fun `finds correct triple`() {
        val result = solver.findTriadWithSum(2020)

        expectThat(result)
            .isNotNull()
            .get { first + second + third } isEqualTo 2020
    }

    @Test
    fun `solves second part`() {
        val result = solver.solveSecondPart()

        expectThat(result) isEqualTo 42275090
    }
}