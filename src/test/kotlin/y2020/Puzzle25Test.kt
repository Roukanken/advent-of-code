package y2020

import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.isEqualTo

internal class Puzzle25Test {

    private fun getSolver() = Puzzle25("/2020/day25.input")

    @Test
    fun `solves first part`() {
        val result = getSolver().solveFirstPart()

        expectThat(result) isEqualTo 9420461
    }

    @Test
    fun `solves second part`() {
        val result = getSolver().solveSecondPart()

        expectThat(result) isEqualTo 3933
    }
}
