package y2020

import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.contains
import strikt.assertions.isEqualTo

internal class Puzzle14Test {

    private fun getSolver() = Puzzle14("/2020/day14.input")

    @Test
    fun `mask works`() {
        val mask = BitmaskStatement.AssignMask.parse("mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X").value

        expectThat(mask) {
            get { apply(11) } isEqualTo 73
            get { apply(101) } isEqualTo 101
            get { apply(0) } isEqualTo 64
        }
    }

    @Test
    fun `mask floating works`() {
        val mask = BitmaskStatement.AssignMask.parse("mask = 000000000000000000000000000000X1001X").value

        expectThat(mask)
            .get { mask.applyV2(42) }
            .get { floating(this) }
            .get { this.toList() }
            .contains(26, 27, 58, 59)

        val mask2 = BitmaskStatement.AssignMask.parse("mask = 00000000000000000000000000000000X0XX").value

        expectThat(mask2)
            .get { mask2.applyV2(26) }
            .get { floating(this) }
            .get { this.toList() }
            .contains(16, 17, 18, 19, 24, 25, 26, 27)
    }


    @Test
    fun `solves first part`() {
        val result = getSolver().solveFirstPart()

        expectThat(result) isEqualTo 13105044880745
    }

    @Test
    fun `solves second part`() {
        val result = getSolver().solveSecondPart()

        expectThat(result) isEqualTo 3505392154485
    }
}
