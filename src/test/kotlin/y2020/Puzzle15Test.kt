package y2020

import org.junit.jupiter.api.Test
import startsWith
import strikt.api.expectThat
import strikt.assertions.isEqualTo

internal class Puzzle15Test {

    private fun getSolver() = Puzzle15("/2020/day15.input")

    @Test
    fun `sequence`() {
        val memoryGame = Puzzle15.memoryGame(listOf(0, 3, 6))

        expectThat(memoryGame).startsWith(0, 3, 6, 0, 3, 3, 1, 0, 4, 0)
    }

    @Test
    fun `solves first part`() {
        val result = getSolver().solveFirstPart()

        expectThat(result) isEqualTo 421
    }

    @Test
    fun `solves second part`() {
        val result = getSolver().solveSecondPart()

        expectThat(result) isEqualTo 436
    }
}
