package y2020

import org.junit.jupiter.api.Test
import strikt.api.Assertion
import strikt.api.expectThat
import strikt.assertions.elementAt
import strikt.assertions.isA
import strikt.assertions.isEqualTo
import y2020.Instruction.*

internal class Puzzle10Test {

    private fun getSolver() = Puzzle10("/2020/day10.input")

    @Test
    fun `solves first part`() {
        val result = getSolver().solveFirstPart()

        expectThat(result) isEqualTo 3034
    }

    @Test
    fun `solves second part`() {
        val result = getSolver().solveSecondPart()

        expectThat(result) isEqualTo 259172170858496
    }
}
