package y2020

import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.isEqualTo

internal class Puzzle21Test {

    private fun getSolver() = Puzzle21("/2020/day21.input")

    @Test
    fun `solves first part`() {
        val result = getSolver().solveFirstPart()

        expectThat(result) isEqualTo "1882"
    }

    @Test
    fun `solves second part`() {
        val result = getSolver().solveSecondPart()

        expectThat(result) isEqualTo "xgtj,ztdctgq,bdnrnx,cdvjp,jdggtft,mdbq,rmd,lgllb"
    }
}
