package y2020

import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.isEqualTo

internal class Puzzle6Test {

    private fun getSolver() = Puzzle6("/2020/day6.input")

    @Test
    fun `solves first part`() {
        val result = getSolver().solveFirstPart()

        expectThat(result) isEqualTo 6947
    }

    @Test
    fun `solves second part`() {
        val result = getSolver().solveSecondPart()

        expectThat(result) isEqualTo 3398
    }
}

