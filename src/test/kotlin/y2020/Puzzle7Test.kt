package y2020

import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.*

internal class Puzzle7Test {

    private fun getSolver() = Puzzle7("/2020/day7.input")

    @Test
    fun `empty bag parses`() {
        val bag = Puzzle7.Bag.parse("faded blue bags contain no other bags.")

        expectThat(bag) {
            get { color } isEqualTo Puzzle7.Color("faded blue")
            get { children }.isEmpty()
        }
    }

    @Test
    fun `full bag parses`() {
        val bag = Puzzle7.Bag.parse("shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.")

        expectThat(bag) {
            get { color } isEqualTo Puzzle7.Color("shiny gold")
            get { children }.containsKeys(Puzzle7.Color("dark olive"), Puzzle7.Color("vibrant plum"))
        }
    }

    @Test
    fun `reverse map`() {
        val parent = Puzzle7.Color("shiny gold")
        val child = Puzzle7.Color("dark olive")

        val bag = Puzzle7.Bag.parse("shiny gold bags contain 3 dark olive bags.")
        val map = Puzzle7.constructReverseMap(mapOf(parent to bag))

        expectThat(map) {
            get { get(child) }
                .isNotNull().contains(parent)
        }
    }

    @Test
    fun `solves first part`() {
        val result = getSolver().solveFirstPart()

        expectThat(result) isEqualTo 197
    }

    @Test
    fun `solves second part`() {
        val result = getSolver().solveSecondPart()

        expectThat(result) isEqualTo 85324
    }
}

