package y2020

import Location
import at
import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.api.expectThrows
import strikt.assertions.all
import strikt.assertions.count
import strikt.assertions.isEqualTo
import y2020.Object.EMPTY
import y2020.Object.TREE

internal class Puzzle3Test {
    @Test
    fun `location add`() {
        val result = Location(2, 5) + Location(9, 10)

        expectThat(result) {
            get { x } isEqualTo 11
            get { y } isEqualTo 15
        }
    }

    @Test
    internal fun `location infix creation`() {
        val result = 5 at 6

        expectThat(result) {
            get { x } isEqualTo 5
            get { y } isEqualTo 6
        }
    }

    @Test
    internal fun `forest throws on uneven sizes`() {
        val map = listOf(listOf(TREE, TREE), listOf(EMPTY))

        expectThrows<IllegalArgumentException> { Forest(map) }
    }

    @Test
    internal fun `forest getting objects`() {
        val forest = Forest.parse(
            """
            #..
            ##.
            """.trimIndent()
        )

        expectThat(forest) {
            get { this[0 at 1] } isEqualTo TREE
            get { this[1 at 0] } isEqualTo EMPTY
            get { this[3 at 1] } isEqualTo TREE
            get { this[4 at 0] } isEqualTo EMPTY
        }
    }

    @Test
    internal fun `forest toboggan`() {
        val forest = Forest.parse(
            """
            #..
            .#.
            """.trimIndent()
        )

        val result = forest.toboggan(1 at 1)

        expectThat(result)
            .get { asIterable() }.and {
                count() isEqualTo 2
                all { isEqualTo(TREE) }
            }
    }

    private fun getSolver() = Puzzle3("/2020/day3.input")

    @Test
    fun `solves first part`() {
        val result = getSolver().solveFirstPart()

        expectThat(result) isEqualTo 254
    }

    @Test
    fun `solves second part`() {
        val result = getSolver().solveSecondPart()

        expectThat(result) isEqualTo 1666768320
    }
}

