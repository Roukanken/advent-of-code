package y2021

import org.junit.jupiter.api.Test
import strikt.api.Assertion
import strikt.api.expectThat
import strikt.assertions.hasSize
import strikt.assertions.isA
import strikt.assertions.isEqualTo
import y2021.Puzzle16.Companion.evaluate

internal class Puzzle16Test {

    @Test
    fun `hexadecimal reader reads bits`() {
        expectThat(HexadecimalReader("D2FE28")) {
            get { readBits(3) } isEqualTo "110"
            get { readBits(5) } isEqualTo "10010"
            get { readBits(1) } isEqualTo "1"
            get { readBits(14) } isEqualTo "11111100010100"
            get { readBits(1) } isEqualTo "0"
        }
    }

    @Test
    fun `hexadecimal reader reads ints`() {
        expectThat(HexadecimalReader("D2FE28")) {
            get { readInt(3) } isEqualTo 6
            get { readInt(3) } isEqualTo 4
            get { readBoolean() } isEqualTo true
            get { readInt(4) } isEqualTo 7
            get { readBoolean() } isEqualTo true
            get { readInt(4) } isEqualTo 14
            get { readBoolean() } isEqualTo false
            get { readInt(4) } isEqualTo 5
        }
    }

    @Test
    fun `bits protocol can read operation by total length`() {
        expectThat(BitsProtocol.read(HexadecimalReader("38006F45291200")))
            .isA<BitsProtocol.Operation>().and {
                get { packets } hasSize 2
                getLiteralValueAt(0) isEqualTo 10
                getLiteralValueAt(1) isEqualTo 20
            }
    }

    @Test
    fun `bits protocol can read operation by packet count`() {
        expectThat(BitsProtocol.read(HexadecimalReader("EE00D40C823060")))
            .isA<BitsProtocol.Operation>().and {
                get { packets } hasSize 3
                getLiteralValueAt(0) isEqualTo 1
                getLiteralValueAt(1) isEqualTo 2
                getLiteralValueAt(2) isEqualTo 3
            }
    }

    @Test
    fun `evaluate works properly`() {
        expectThat("C200B40A82").readAndEval() isEqualTo 3
        expectThat("04005AC33890").readAndEval() isEqualTo 54
        expectThat("880086C3E88112").readAndEval() isEqualTo 7
        expectThat("CE00C43D881120").readAndEval() isEqualTo 9
        expectThat("D8005AC2A8F0").readAndEval() isEqualTo 1
        expectThat("F600BC2D8F").readAndEval() isEqualTo 0
        expectThat("9C005AC2F8F0").readAndEval() isEqualTo 0
        expectThat("9C0141080250320F1802104A08").readAndEval() isEqualTo 1
    }

    @Test
    fun `solves first part`() {
        val result = Puzzle16("/2021/day16.input").solveFirstPart()

        expectThat(result) isEqualTo 854
    }

    @Test
    fun `solves second part`() {
        val result = Puzzle16("/2021/day16.input").solveSecondPart()

        expectThat(result) isEqualTo 186189840660
    }
}

infix fun Assertion.Builder<BitsProtocol.Operation>.getLiteralValueAt(position: Int) =
    get { packets[position] }.describedAs("packets[$position]")
        .isA<BitsProtocol.Literal>().get { value }

fun Assertion.Builder<String>.readAndEval() =
    get { BitsProtocol.read(HexadecimalReader(this)) }.describedAs("BitsOperation.read()")
        .get { evaluate() }