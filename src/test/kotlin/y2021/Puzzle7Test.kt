package y2021

import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.isEqualTo
import strikt.assertions.isIn
import y2021.Puzzle7.Companion.fuelDistanceTo
import y2021.Puzzle7.Companion.realFuelDistanceTo
import y2021.Puzzle7.Companion.sweepRealCrabs

internal class Puzzle7Test {

    @Test
    fun `quick select works`() {
        val list = listOf(5, 4, 7, 2, 8, 3, 1, 6, 9, 8, 0)

        expectThat(list) {
            get { quickSelect(4) } isEqualTo 4
            get { quickSelect(8) } isEqualTo 8
            get { quickSelect(9) } isEqualTo 8
            get { quickSelect(10) } isEqualTo 9
        }
    }

    @Test
    fun `fuel distance`() {
        val distances = listOf(16, 1, 2, 0, 4, 2, 7, 1, 2, 14)

        expectThat(distances) {
            get { fuelDistanceTo(2) } isEqualTo 37
            get { fuelDistanceTo(1) } isEqualTo 41
            get { fuelDistanceTo(3) } isEqualTo 39
        }
    }

    @Test
    fun `real fuel distance`() {
        val distances = listOf(16, 1, 2, 0, 4, 2, 7, 1, 2, 14)

        expectThat(distances) {
            get { realFuelDistanceTo(2) } isEqualTo 206
            get { realFuelDistanceTo(5) } isEqualTo 168
        }
    }

    @Test
    fun `sweep real crabs`() {
        val distances = listOf(16, 1, 2, 0, 4, 2, 7, 1, 2, 14)

        expectThat(distances) {
            get { sweepRealCrabs() } isIn 4..5
        }
    }

    @Test
    fun `solves first part`() {
        val result = Puzzle7("/2021/day7.input").solveFirstPart()

        expectThat(result) isEqualTo 359648
    }

    @Test
    fun `solves second part`() {
        val result = Puzzle7("/2021/day7.input").solveSecondPart()

        expectThat(result) isEqualTo 100727924
    }
}