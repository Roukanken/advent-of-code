package y2021

import arrow.core.Either
import org.junit.jupiter.api.Test
import strikt.api.expect
import strikt.api.expectThat
import strikt.assertions.isEqualTo
import y2021.Puzzle10.Companion.validateParenthesis
import y2021.Puzzle10.Parenthesis

internal class Puzzle10Test {

    @Test
    fun `illegal closing`() {
        expect {
            that("{([(<{}[<>[]}>{[]{[(<()>").get { validateParenthesis() } isEqualTo Either.Left(Parenthesis.CLOSING_SQUIRLY)
            that("[[<[([]))<([[{}[[()]]]").get { validateParenthesis() } isEqualTo Either.Left(Parenthesis.CLOSING_NORMAL)
            that("[{[{({}]{}}([{[{{{}}([]").get { validateParenthesis() } isEqualTo Either.Left(Parenthesis.CLOSING_SQUARE)
            that("[<(<(<(<{}))><([]([]()").get { validateParenthesis() } isEqualTo Either.Left(Parenthesis.CLOSING_NORMAL)
            that("<{([([[(<>()){}]>(<<{{").get { validateParenthesis() } isEqualTo Either.Left(Parenthesis.CLOSING_ANGLED)
        }
    }

    @Test
    fun `solves first part`() {
        val result = Puzzle10("/2021/day10.input").solveFirstPart()

        expectThat(result) isEqualTo 394647
    }

    @Test
    fun `solves second part`() {
        val result = Puzzle10("/2021/day10.input").solveSecondPart()

        expectThat(result) isEqualTo 2380061249
    }
}