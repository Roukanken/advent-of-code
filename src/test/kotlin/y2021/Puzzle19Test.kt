package y2021

import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.isEqualTo

internal class Puzzle19Test {
    @Test
    fun `solves first part`() {
        val result = Puzzle19("/2021/day19.input").solveFirstPart()

        expectThat(result) isEqualTo 609
    }

    @Test
    fun `solves second part`() {
        val result = Puzzle19("/2021/day19.input").solveSecondPart()

        expectThat(result) isEqualTo 2925
    }
}