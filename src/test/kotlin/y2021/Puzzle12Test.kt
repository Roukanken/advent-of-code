package y2021

import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.isEqualTo

internal class Puzzle12Test {
    @Test
    fun `solves first part`() {
        val result = Puzzle12("/2021/day12.input").solveFirstPart()

        expectThat(result) isEqualTo 4549
    }

    @Test
    fun `solves second part`() {
        val result = Puzzle12("/2021/day12.input").solveSecondPart()

        expectThat(result) isEqualTo 120535
    }
}