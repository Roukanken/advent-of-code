package y2021

import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.isEqualTo

internal class Puzzle9Test {
    @Test
    fun `solves first part`() {
        val result = Puzzle9("/2021/day9.input").solveFirstPart()

        expectThat(result) isEqualTo 425
    }

    @Test
    fun `solves second part`() {
        val result = Puzzle9("/2021/day9.input").solveSecondPart()

        expectThat(result) isEqualTo 1135260
    }
}