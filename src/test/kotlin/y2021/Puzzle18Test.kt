package y2021

import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.isEqualTo
import y2021.SnailFishNumber.Companion.toSnailFishNumber

internal class Puzzle18Test {
    @Test
    fun `addition works`() {
        val left = "[[[[4,3],4],4],[7,[[8,4],9]]]".toSnailFishNumber()
        val right = "[1,1]".toSnailFishNumber()

        expectThat(left + right) isEqualTo "[[[0,7],4],[[7,8],[6,0]]],[8,1]]".toSnailFishNumber()
    }

    @Test
    fun `solves first part`() {
        val result = Puzzle18("/2021/day18.input").solveFirstPart()

        expectThat(result) isEqualTo 4145
    }

    @Test
    fun `solves second part`() {
        val result = Puzzle18("/2021/day18.input").solveSecondPart()

        expectThat(result) isEqualTo 4855
    }
}