package y2021

import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.isEqualTo

internal class Puzzle3Test {

    val sampleInput = listOf(
        "00100",
        "11110",
        "10110",
        "10111",
        "10101",
        "01111",
        "00111",
        "11100",
        "10000",
        "11001",
        "00010",
        "01010",
    )

    @Test
    fun `fuzzy binary work`() {
        val fuzzies = sampleInput.map { FuzzyBinaryNumber(it) }

        expectThat(fuzzies)
            .get { reduce { a, b -> a + b } } and {
            get { toInt() } isEqualTo 22
            get { toString() } isEqualTo "0fb10110"
            get { (!this).toInt() } isEqualTo 9
            get { (!this).toString() } isEqualTo "0fb01001"
        }
    }

    @Test
    fun `filter by bit criteria works`() {
        with(Puzzle3) {
            expectThat(sampleInput) {
                get { findOxygenGenerator() } isEqualTo "10111"
                get { findCo2Scrubber() } isEqualTo "01010"
                get { filterByBitCriteria { true } } isEqualTo "11110"
                get { filterByBitCriteria { false } } isEqualTo "00010"
            }
        }
    }

    @Test
    fun `solves first part`() {
        val result = Puzzle3("/2021/day3.input").solveFirstPart()

        expectThat(result) isEqualTo 4001724
    }

    @Test
    fun `solves second part`() {
        val result = Puzzle3("/2021/day3.input").solveSecondPart()

        expectThat(result) isEqualTo 587895
    }
}