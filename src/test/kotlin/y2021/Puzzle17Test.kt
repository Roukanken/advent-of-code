package y2021

import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.isEqualTo

internal class Puzzle17Test {
    @Test
    fun `solves first part`() {
        val result = Puzzle17("/2021/day17.input").solveFirstPart()

        expectThat(result) isEqualTo 4278
    }

    @Test
    fun `solves second part`() {
        val result = Puzzle17("/2021/day17.input").solveSecondPart()

        expectThat(result) isEqualTo 1994
    }
}