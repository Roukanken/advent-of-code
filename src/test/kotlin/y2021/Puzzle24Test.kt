package y2021

import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.isEqualTo

internal class Puzzle24Test {
    @Test
    fun `solves first part`() {
        val result = Puzzle24("/2021/day24.input").solveFirstPart()

        expectThat(result) isEqualTo 609
    }

    @Test
    fun `solves second part`() {
        val result = Puzzle24("/2021/day24.input").solveSecondPart()

        expectThat(result) isEqualTo 2925
    }
}