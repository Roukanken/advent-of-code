package y2021

import org.junit.jupiter.api.Test
import strikt.api.expect
import strikt.api.expectThat
import strikt.assertions.isEqualTo
import y2021.SubmarineCommand.*
import y2021.SubmarineCommand.Companion.toSubmarineCommand

internal class Puzzle2Test {

    @Test
    fun `submarine can execute command`() {
        expectThat(Submarine(42, 47)) {
            get { execute(Forward(5)) } isEqualTo Submarine(47, 47)
            get { execute(Down(5)) } isEqualTo Submarine(42, 52)
            get { execute(Up(5)) } isEqualTo Submarine(42, 42)
        }
    }

    @Test
    fun `submarine can execute list of commands`() {
        val commands = listOf(
            Forward(5),
            Down(5),
            Forward(8),
            Up(3),
            Down(8),
            Forward(2),
        )

        expectThat(Submarine(0, 0))
            .get { execute(commands) }
            .isEqualTo(Submarine(15, 10))
    }

    @Test
    fun `submarine can execute aimed command`() {
        expectThat(Submarine(10, 10, 5)) {
            get { aimedExecute(Forward(5)) } isEqualTo Submarine(15, 35, 5)
            get { aimedExecute(Down(5)) } isEqualTo Submarine(10, 10, 10)
            get { aimedExecute(Up(5)) } isEqualTo Submarine(10, 10, 0)
        }
    }

    @Test
    fun `submarine can execute list of aimed commands`() {
        val commands = listOf(
            Forward(5),
            Down(5),
            Forward(8),
            Up(3),
            Down(8),
            Forward(2),
        )

        expectThat(Submarine(0, 0))
            .get { aimedExecute(commands) }
            .isEqualTo(Submarine(15, 60, 10))
    }


    @Test
    fun `can parse commands`() {
        expect {
            that("forward 5").get { toSubmarineCommand() }.isEqualTo(Forward(5))
            that("down 45").get { toSubmarineCommand() }.isEqualTo(Down(45))
            that("up 89").get { toSubmarineCommand() }.isEqualTo(Up(89))
        }
    }

    @Test
    fun `solves first part`() {
        val result = Puzzle2("/2021/day2.input").solveFirstPart()

        expectThat(result) isEqualTo 1636725
    }

    @Test
    fun `solves second part`() {
        val result = Puzzle2("/2021/day2.input").solveSecondPart()

        expectThat(result) isEqualTo 1872757425
    }
}