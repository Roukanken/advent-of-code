package y2021

import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.isEqualTo
import strikt.assertions.isNotNull
import y2021.Puzzle6.Companion.simulateAll

internal class Puzzle6Test {

    @Test
    fun `fish simulate properly`() {
        val daysLeft = listOf(3, 4, 3, 1, 2)
        val fishes = daysLeft.map { Fish(it) }.count()

        expectThat(fishes) {
            get { simulateAll().take(18).lastOrNull() }.isNotNull().get { values.sum() }.isEqualTo(26)
            get { simulateAll().take(80).lastOrNull() }.isNotNull().get { values.sum() }.isEqualTo(5934)
        }
    }

    @Test
    fun `solves first part`() {
        val result = Puzzle6("/2021/day6.input").solveFirstPart()

        expectThat(result) isEqualTo 391888
    }

    @Test
    fun `solves second part`() {
        val result = Puzzle6("/2021/day6.input").solveSecondPart()

        expectThat(result) isEqualTo 1754597645339
    }
}