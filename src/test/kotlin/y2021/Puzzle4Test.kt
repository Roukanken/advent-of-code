package y2021

import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.isEqualTo
import strikt.assertions.isSuccess
import y2021.BingoBoard.Companion.toBingoBoard

internal class Puzzle4Test {

    @Test
    fun `bingo board parses`() {
        val data = """
            22 13 17 11  0
             8  2 23  4 24
            21  9 14 16  7
             6 10  3 18  5
             1 12 20 15 19
        """.trimIndent()

        expectThat(data)
            .get { runCatching { toBingoBoard() } }
            .isSuccess()
    }

    @Test
    fun `input parses`() {
        val data = """
            1,42,8,3,9
            
            22 13 17 11  0
             8  2 23  4 24
            21  9 14 16  7
             6 10  3 18  5
             1 12 20 15 19
        """.trimIndent()

        with(Puzzle4.Input) {
            expectThat(data)
                .get { runCatching { toInput() } }
                .isSuccess()
        }
    }

    @Test
    fun `lottery numbers can get time when all are drawn`() {
        expectThat(LotteryNumbers(listOf(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10)))
            .get { getTimeWhenAllGuessedAreDrawn(listOf(4, 1, 3, 8, 5)) }
            .isEqualTo(8)
    }

    @Test
    fun `bingo board can find win time`() {
        val board = """
            14 21 17 24  4
            10 16 15  9 19
            18  8 23 26 20
            22 11 13  6  5
             2  0 12  3  7
        """.trimIndent().toBingoBoard()

        expectThat(board)
            .get { getWinTime(LotteryNumbers(listOf(7, 4, 9, 5, 11, 17, 23, 2, 0, 14, 21, 24, 10, 16, 13))) }
            .isEqualTo(11)
    }

    @Test
    fun `solves first part`() {
        val result = Puzzle4("/2021/day4.input").solveFirstPart()

        expectThat(result) isEqualTo 39902
    }

    @Test
    fun `solves second part`() {
        val result = Puzzle4("/2021/day4.input").solveSecondPart()

        expectThat(result) isEqualTo 26936
    }
}