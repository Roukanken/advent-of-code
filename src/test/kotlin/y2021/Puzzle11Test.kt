package y2021

import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.isEqualTo

internal class Puzzle11Test {
    @Test
    fun `solves first part`() {
        val result = Puzzle11("/2021/day11.input").solveFirstPart()

        expectThat(result) isEqualTo 1721
    }

    @Test
    fun `solves second part`() {
        val result = Puzzle11("/2021/day11.input").solveSecondPart()

        expectThat(result) isEqualTo 298
    }
}