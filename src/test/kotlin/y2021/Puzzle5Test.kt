package y2021

import at
import org.junit.jupiter.api.Test
import strikt.api.expect
import strikt.api.expectThat
import strikt.assertions.isEqualTo
import y2021.Segment.Companion.toSegment

internal class Puzzle5Test {

    @Test
    fun `segment parses`() {
        expectThat("0,9 -> 5,9")
            .get { toSegment() }
            .isEqualTo(Segment(0 at 9, 5 at 9))
    }

    @Test
    fun `counter counts properly`() {
        val counter = Counter<String>()
        counter.addAll(listOf("1", "2", "3", "4", "5", "2", "3", "4", "5", "3", "4", "5", "4", "5", "5"))

        expectThat(counter) isEqualTo Counter(mutableMapOf("1" to 1, "2" to 2, "3" to 3, "4" to 4, "5" to 5))
    }

    @Test
    fun `segment can determine it's angle`() {
        expect {
            that(Segment(10 at 10, 45 at 25)).get { angle } isEqualTo (7 at 3)
            that(Segment(10 at 10, -25 at 25)).get { angle } isEqualTo (-7 at 3)
            that(Segment(10 at 10, 10 at 25)).get { angle } isEqualTo (0 at 1)
            that(Segment(10 at 10, -10 at 25)).get { angle } isEqualTo (-4 at 3)
        }
    }

    @Test
    fun `segment can determine it's points`() {
        expect {
            that(Segment(1 at 1, 1 at 3)).get { points } isEqualTo listOf(1 at 1, 1 at 2, 1 at 3)
            that(Segment(9 at 7, 7 at 7)).get { points } isEqualTo listOf(9 at 7, 8 at 7, 7 at 7)
            that(Segment(10 at 10, 45 at 25)).get { points } isEqualTo listOf(10 at 10, 17 at 13, 24 at 16, 31 at 19, 38 at 22, 45 at 25)
        }
    }

    @Test
    fun `solves first part`() {
        val result = Puzzle5("/2021/day5.input").solveFirstPart()

        expectThat(result) isEqualTo 7438
    }

    @Test
    fun `solves second part`() {
        val result = Puzzle5("/2021/day5.input").solveSecondPart()

        expectThat(result) isEqualTo 21406
    }
}