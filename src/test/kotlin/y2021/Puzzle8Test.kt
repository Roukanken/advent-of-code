package y2021

import org.junit.jupiter.api.Test
import strikt.api.expectCatching
import strikt.api.expectThat
import strikt.assertions.isContainedIn
import strikt.assertions.isEqualTo
import strikt.assertions.isSuccess
import y2021.Puzzle8.Companion.identifyReverseMapping
import y2021.Puzzle8.DisplaySegment

internal class Puzzle8Test {

    @Test
    fun `input parses`() {
        with(Puzzle8.Input) {
            expectCatching { "be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe".toInput() }
                .isSuccess()
        }
    }

    @Test
    fun `basic identification works`() {
        with(Puzzle8.Input) {
            val definitions = "be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe"
                .toInput()
                .definitions

            println(definitions.identifyReverseMapping())

            expectThat(definitions).get { identifyReverseMapping() } and {
                val one = listOf(DisplaySegment.C, DisplaySegment.F)
                get { this[DisplaySegment.B] } isContainedIn one
                get { this[DisplaySegment.E] } isContainedIn one

                val four = setOf(DisplaySegment.B, DisplaySegment.C, DisplaySegment.D, DisplaySegment.F)
                get { this[DisplaySegment.G] } isContainedIn four
                get { this[DisplaySegment.C] } isContainedIn four
                get { this[DisplaySegment.B] } isContainedIn four
                get { this[DisplaySegment.E] } isContainedIn four

                val seven = listOf(DisplaySegment.A, DisplaySegment.C, DisplaySegment.F)
                get { this[DisplaySegment.E] } isContainedIn seven
                get { this[DisplaySegment.D] } isContainedIn seven
                get { this[DisplaySegment.B] } isContainedIn seven
            }
        }
    }

    @Test
    fun `solves first part`() {
        val result = Puzzle8("/2021/day8.input").solveFirstPart()

        expectThat(result) isEqualTo 301
    }

    @Test
    fun `solves second part`() {
        val result = Puzzle8("/2021/day8.input").solveSecondPart()

        expectThat(result) isEqualTo 908067
    }
}