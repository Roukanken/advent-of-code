package y2021

import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.isEqualTo

internal class Puzzle1Test {

    @Test
    fun `can count increases`() {
        with(Puzzle1) {
            expectThat(listOf(199, 200, 208, 210, 200, 207, 240, 269, 260, 263))
                .get { countIncreases() }
                .isEqualTo(7)
        }
    }

    @Test
    fun `solves first part`() {
        val result = Puzzle1("/2021/day1.input").solveFirstPart()

        expectThat(result) isEqualTo 1316
    }

    @Test
    fun `can count windowed increases`() {
        with(Puzzle1) {
            expectThat(listOf(199, 200, 208, 210, 200, 207, 240, 269, 260, 263))
                .get { countWindowedIncreases(3) }
                .isEqualTo(5)
        }
    }

    @Test
    fun `solves second part`() {
        val result = Puzzle1("/2021/day1.input").solveSecondPart()

        expectThat(result) isEqualTo 1344
    }
}

internal class Puzzle1AlternativeTest {

    private val solver = Puzzle1Alternative("/2021/day1.input")

    @Test
    fun `can count increases`() {
        with(Puzzle1Alternative) {
            expectThat(listOf(199, 200, 208, 210, 200, 207, 240, 269, 260, 263))
                .get { countIncreasesOfSums(1) }
                .isEqualTo(7)
        }
    }

    @Test
    fun `solves first part`() {
        val result = solver.solveFirstPart()

        expectThat(result) isEqualTo 1316
    }

    @Test
    fun `can count windowed increases`() {
        with(Puzzle1Alternative) {
            expectThat(listOf(199, 200, 208, 210, 200, 207, 240, 269, 260, 263))
                .get { countIncreasesOfSums(3) }
                .isEqualTo(5)
        }
    }

    @Test
    fun `solves second part`() {
        val result = solver.solveSecondPart()

        expectThat(result) isEqualTo 1344
    }
}