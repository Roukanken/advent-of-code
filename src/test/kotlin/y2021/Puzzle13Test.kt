package y2021

import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.isEqualTo

internal class Puzzle13Test {
    @Test
    fun `solves first part`() {
        val result = Puzzle13("/2021/day13.input").solveFirstPart()

        expectThat(result) isEqualTo 842
    }

    @Test
    fun `solves second part`() {
        val result = Puzzle13("/2021/day13.input").solveSecondPart()

        expectThat(result) isEqualTo """
            ███  ████ █  █ ███   ██    ██ ████ █  █
            █  █ █    █ █  █  █ █  █    █    █ █  █
            ███  ███  ██   █  █ █       █   █  █  █
            █  █ █    █ █  ███  █       █  █   █  █
            █  █ █    █ █  █ █  █  █ █  █ █    █  █
            ███  █    █  █ █  █  ██   ██  ████  ██ 
        """.trimIndent()
    }
}