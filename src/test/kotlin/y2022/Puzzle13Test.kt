package y2022

import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.isEqualTo

internal class Puzzle13Test {

    @Test
    fun `solves first part`() {
        val result = Puzzle13("/2022/day13.input").solveFirstPart()

        expectThat(result) isEqualTo 5588
    }

    @Test
    fun `solves second part`() {
        val result = Puzzle13("/2022/day13.input").solveSecondPart()

        expectThat(result) isEqualTo 0
    }
}
