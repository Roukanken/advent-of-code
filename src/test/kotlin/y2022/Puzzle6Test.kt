package y2022

import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.isEqualTo

internal class Puzzle6Test {

    @Test
    fun `solves first part`() {
        val result = Puzzle6("/2022/day6.input").solveFirstPart()

        expectThat(result) isEqualTo 1140
    }

    @Test
    fun `solves second part`() {
        val result = Puzzle6("/2022/day6.input").solveSecondPart()

        expectThat(result) isEqualTo 3495
    }
}
