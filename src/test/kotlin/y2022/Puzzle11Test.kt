package y2022

import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.isEqualTo

internal class Puzzle11Test {

    @Test
    fun `solves first part`() {
        val result = Puzzle11("/2022/day11.input").solveFirstPart()

        expectThat(result) isEqualTo 90294L
    }

    @Test
    fun `solves second part`() {
        val result = Puzzle11("/2022/day11.input").solveSecondPart()

        expectThat(result) isEqualTo 18170818354L
    }
}
