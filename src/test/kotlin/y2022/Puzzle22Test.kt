package y2022

import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.isEqualTo

internal class Puzzle22Test {

    @Test
    fun `solves first part`() {
        val result = Puzzle22("/2022/day22.input").solveFirstPart()

        expectThat(result) isEqualTo 0
    }

    @Test
    fun `solves second part`() {
        val result = Puzzle22("/2022/day22.input").solveSecondPart()

        expectThat(result) isEqualTo 0
    }
}
