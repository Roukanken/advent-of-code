package y2022

import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.isEqualTo

internal class Puzzle8Test {

    @Test
    fun `solves first part`() {
        val result = Puzzle8("/2022/day8.input").solveFirstPart()

        expectThat(result) isEqualTo 1823
    }

    @Test
    fun `solves second part`() {
        val result = Puzzle8("/2022/day8.input").solveSecondPart()

        expectThat(result) isEqualTo 211680
    }
}
