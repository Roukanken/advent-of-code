package y2022

import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.isEqualTo

internal class Puzzle17Test {

    @Test
    fun `solves first part`() {
        val result = Puzzle17("/2022/day17.input").solveFirstPart()

        expectThat(result) isEqualTo 0
    }

    @Test
    fun `solves second part`() {
        val result = Puzzle17("/2022/day17.input").solveSecondPart()

        expectThat(result) isEqualTo 0
    }
}
