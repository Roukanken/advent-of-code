package y2022

import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.isEqualTo

internal class Puzzle19Test {

    @Test
    fun `solves first part`() {
        val result = Puzzle19("/2022/day19.input").solveFirstPart()

        expectThat(result) isEqualTo 0
    }

    @Test
    fun `solves second part`() {
        val result = Puzzle19("/2022/day19.input").solveSecondPart()

        expectThat(result) isEqualTo 0
    }
}
