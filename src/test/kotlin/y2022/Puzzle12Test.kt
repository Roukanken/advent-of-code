package y2022

import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.isEqualTo

internal class Puzzle12Test {

    @Test
    fun `solves first part`() {
        val result = Puzzle12("/2022/day12.input").solveFirstPart()

        expectThat(result) isEqualTo 468
    }

    @Test
    fun `solves second part`() {
        val result = Puzzle12("/2022/day12.input").solveSecondPart()

        expectThat(result) isEqualTo 459
    }
}
