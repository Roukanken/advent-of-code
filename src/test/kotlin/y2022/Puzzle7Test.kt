package y2022

import org.junit.jupiter.api.Test
import strikt.api.Assertion
import strikt.api.expectThat
import strikt.assertions.all
import strikt.assertions.contains
import strikt.assertions.isA
import strikt.assertions.isEqualTo
import y2022.FilesystemCommand.ListFiles.Entry.Directory
import y2022.FilesystemCommand.ListFiles.Entry.File

internal class Puzzle7Test {

    private val testInput = """
        $ cd /
        $ ls
        dir a
        14848514 b.txt
        8504156 c.dat
        dir d
        $ cd a
        $ ls
        dir e
        29116 f
        2557 g
        62596 h.lst
        $ cd e
        $ ls
        584 i
        $ cd ..
        $ cd ..
        $ cd d
        $ ls
        4060174 j
        8033020 d.log
        5626152 d.ext
        7214296 k
    """.trimIndent()

    @Test
    fun `reads test input correctly`() {
        val commands = FilesystemCommand.parseMany(testInput)

        expectThat(commands) {
            get { this[0] } isEqualTo FilesystemCommand.ChangeDirectory.ToRoot
            get { this[1] }.and {
                isA<FilesystemCommand.ListFiles>()
                    .get { result }.contains(
                        File("b.txt", 14848514),
                        File("c.dat", 8504156),
                        Directory("a"),
                        Directory("d")
                    )
            }
            get { this[2] } isEqualTo FilesystemCommand.ChangeDirectory.ToSubdirectory("a")
            get { this[6] } isEqualTo FilesystemCommand.ChangeDirectory.ToOneLevelUp
        }
    }

    @Test
    fun `can reconstruct filesystem`() {
        val filesystem = reconstructFilesystem {
            FilesystemCommand.parseMany(testInput).forEach { it.execute() }
        }

        expectThat(filesystem).isDirectory().and {
            get { entries[0] }.isDirectory().and {
                get { name } isEqualTo "a"
                get { entries[0] }.isDirectory().and {
                    get { name } isEqualTo "e"
                    get { entries[0] }.isFile() isEqualTo FilesystemEntry.File("i", 584)
                }
            }
            get { entries[3] }.isDirectory().and {
                get { name } isEqualTo "d"
                get { entries }.all { isFile() }
            }
        }
    }

    private fun Assertion.Builder<FilesystemEntry>.isFile() = isA<FilesystemEntry.File>()
    private fun Assertion.Builder<FilesystemEntry>.isDirectory() = isA<FilesystemEntry.Directory>()

    @Test
    fun `solves first part`() {
        val result = Puzzle7("/2022/day7.input").solveFirstPart()

        expectThat(result) isEqualTo 1423358
    }

    @Test
    fun `solves second part`() {
        val result = Puzzle7("/2022/day7.input").solveSecondPart()

        expectThat(result) isEqualTo 545729
    }
}
