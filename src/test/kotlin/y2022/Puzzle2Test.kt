package y2022

import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.isEqualTo

internal class Puzzle2Test {

    @Test
    fun `solves first part`() {
        val result = Puzzle2("/2022/day2.input").solveFirstPart()

        expectThat(result) isEqualTo 10624
    }

    @Test
    fun `solves second part`() {
        val result = Puzzle2("/2022/day2.input").solveSecondPart()

        expectThat(result) isEqualTo 14060
    }
}
