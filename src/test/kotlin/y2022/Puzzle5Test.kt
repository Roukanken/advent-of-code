package y2022

import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.first
import strikt.assertions.hasSize
import strikt.assertions.isEqualTo
import strikt.assertions.last

internal class Puzzle5Test {

    private val exampleInput = """
                    [D]    
                [N] [C]    
                [Z] [M] [P]
                 1   2   3 
                
                move 1 from 2 to 1
                move 3 from 1 to 3
                move 2 from 2 to 1
                move 1 from 1 to 2
            """
        .trimIndent()

    @Test
    fun `parses test input properly`() {
        val input = with(Puzzle5) { exampleInput.parseInput() }

        expectThat(input) {
            get { shipyard }.and {
                get { stacks } hasSize 3
                get { stacks[1].crates } isEqualTo listOf(Shipyard.Crate('M'), Shipyard.Crate('C'), Shipyard.Crate('D'))
            }

            get { craneInstructions }.and {
                hasSize(4)
                first() isEqualTo CraneInstruction(1, 1, 0)
                last() isEqualTo CraneInstruction(1, 0, 1)
            }
        }
    }

    @Test
    fun `solves first part`() {
        val result = Puzzle5("/2022/day5.input").solveFirstPart()

        expectThat(result) isEqualTo "LJSVLTWQM"
    }

    @Test
    fun `solves second part`() {
        val result = Puzzle5("/2022/day5.input").solveSecondPart()

        expectThat(result) isEqualTo "BRQWDBBJM"
    }
}
