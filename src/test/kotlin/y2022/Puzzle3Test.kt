package y2022

import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.isEqualTo

internal class Puzzle3Test {

    @Test
    fun `solves first part`() {
        val result = Puzzle3("/2022/day3.input").solveFirstPart()

        expectThat(result) isEqualTo 7674
    }

    @Test
    fun `solves second part`() {
        val result = Puzzle3("/2022/day3.input").solveSecondPart()

        expectThat(result) isEqualTo 2805
    }
}
