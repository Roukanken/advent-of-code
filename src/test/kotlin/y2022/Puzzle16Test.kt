package y2022

import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.isEqualTo

internal class Puzzle16Test {

    @Test
    fun `solves first part`() {
        val result = Puzzle16("/2022/day16.input").solveFirstPart()

        expectThat(result) isEqualTo 0
    }

    @Test
    fun `solves second part`() {
        val result = Puzzle16("/2022/day16.input").solveSecondPart()

        expectThat(result) isEqualTo 0
    }
}
