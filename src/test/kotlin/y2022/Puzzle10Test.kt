package y2022

import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.isEqualTo

internal class Puzzle10Test {

    @Test
    fun `solves first part`() {
        val result = Puzzle10("/2022/day10.input").solveFirstPart()

        expectThat(result) isEqualTo 14160
    }

    @Test
    fun `solves second part`() {
        val result = Puzzle10("/2022/day10.input").solveSecondPart()

        expectThat(result.trim()) isEqualTo """
            .###....##.####.###..###..####.####..##.
            .#..#....#.#....#..#.#..#.#....#....#..#
            .#..#....#.###..#..#.#..#.###..###..#...
            .###.....#.#....###..###..#....#....#...
            .#.#..#..#.#....#.#..#....#....#....#..#
            .#..#..##..####.#..#.#....####.#.....##.
        """.trimIndent()
    }
}
