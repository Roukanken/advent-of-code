package y2022

import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.isEqualTo

internal class Puzzle15Test {

    @Test
    fun `solves first part`() {
        val result = Puzzle15("/2022/day15.input").solveFirstPart()

        expectThat(result) isEqualTo 0
    }

    @Test
    fun `solves second part`() {
        val result = Puzzle15("/2022/day15.input").solveSecondPart()

        expectThat(result) isEqualTo 0
    }
}
