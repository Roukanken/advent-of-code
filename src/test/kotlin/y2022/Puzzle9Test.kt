package y2022

import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.isEqualTo

internal class Puzzle9Test {

    @Test
    fun `solves first part`() {
        val result = Puzzle9("/2022/day9.input").solveFirstPart()

        expectThat(result) isEqualTo 6212
    }

    @Test
    fun `solves second part`() {
        val result = Puzzle9("/2022/day9.input").solveSecondPart()

        expectThat(result) isEqualTo 0
    }
}
