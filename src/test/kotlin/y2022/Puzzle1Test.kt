package y2022

import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.isEqualTo

internal class Puzzle1Test {

    @Test
    fun `solves first part`() {
        val result = Puzzle1("/2022/day1.input").solveFirstPart()

        expectThat(result) isEqualTo 72070
    }

    @Test
    fun `solves second part`() {
        val result = Puzzle1("/2022/day1.input").solveSecondPart()

        expectThat(result) isEqualTo 211805
    }
}
