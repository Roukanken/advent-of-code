package y2022

import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.isEqualTo

internal class Puzzle4Test {

    @Test
    fun `solves first part`() {
        val result = Puzzle4("/2022/day4.input").solveFirstPart()

        expectThat(result) isEqualTo 471
    }

    @Test
    fun `solves second part`() {
        val result = Puzzle4("/2022/day4.input").solveSecondPart()

        expectThat(result) isEqualTo 888
    }
}
