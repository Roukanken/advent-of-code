import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.contains
import strikt.assertions.isEqualTo

internal class UtilsTest {
    @Test
    fun `location neighborhood`() {
        expectThat((5 at 5).neighborhood()).get { toList() }
            .contains(
                4 at 4,
                4 at 5,
                4 at 6,
                5 at 4,
                5 at 6,
                6 at 4,
                6 at 5,
                6 at 6,
            )
    }

    @Test
    fun `transpose list`() {
        expectThat(
            listOf(
                listOf(1, 2, 3),
                listOf(4, 5, 6),
            )
        ).get { transpose() }
            .isEqualTo(
                listOf(
                    listOf(1, 4),
                    listOf(2, 5),
                    listOf(3, 6),
                )
            )
    }
}