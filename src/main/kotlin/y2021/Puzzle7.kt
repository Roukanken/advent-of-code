package y2021

import Puzzle
import UnreachableCode
import java.io.BufferedReader
import kotlin.math.absoluteValue

class Puzzle7(path: String) : Puzzle<List<Int>, Int>(path) {
    override fun parseInput(reader: BufferedReader) = reader.readLine()
        .trim()
        .split(",")
        .map { it.toInt() }

    override fun solveFirstPart(): Int {
        return listOf(input.quickSelect(input.size / 2), input.quickSelect(input.size / 2 + 1))
            .minOf { input.fuelDistanceTo(it ?: 0) }
    }

    override fun solveSecondPart(): Int {
        val target = input.sweepRealCrabs()

        return listOf(target, target + 1)
            .minOf { input.realFuelDistanceTo(it) }
    }

    companion object {
        fun List<Int>.sweepRealCrabs(): Int {
            val sorted = sorted()

            var rightDistance = sorted.fuelDistanceTo(sorted[0])
            var rightCrabs = sorted.size - 1
            var leftDistance = 0
            var leftCrabs = 1

            for ((left, right) in sorted.windowed(2)) {
                val diff = right - left

                if (leftDistance + leftCrabs * diff < rightDistance - rightCrabs * diff) {
                    leftDistance += leftCrabs * diff
                    rightDistance -= rightCrabs * diff
                    leftCrabs += 1
                    rightCrabs -= 1
                } else {
                    return left + (rightDistance - leftDistance) / (leftCrabs + rightCrabs)
                }
            }

            throw UnreachableCode()
        }

        fun List<Int>.fuelDistanceTo(point: Int): Int = sumOf { (it - point).absoluteValue }
        fun List<Int>.realFuelDistanceTo(point: Int): Int = sumOf {
            val distance = (it - point).absoluteValue
            (distance + 1) * distance / 2
        }
    }
}

fun <T : Comparable<T>> List<T>.quickSelect(kthLowest: Int): T {
    require(kthLowest <= size) { "kthLowest must be less than array size ($size) but was $kthLowest" }
    if (size == 1) return this[0]

    val pivot = this.last()
    val (less, more) = this.filter { it != pivot }.partition { it < pivot }
    return when {
        kthLowest < less.size -> less.quickSelect(kthLowest)
        kthLowest < size - more.size -> pivot
        else -> more.quickSelect(kthLowest - (size - more.size))
    }
}

fun <T : Comparable<T>> List<T>.median(): T = quickSelect(size / 2)