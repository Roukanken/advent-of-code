package y2021

import Dynamic
import Puzzle
import fold
import y2021.Puzzle14.Input.Companion.toInput
import y2021.Puzzle14.Rule.Companion.toRule
import java.io.BufferedReader

class Puzzle14(path: String) : Puzzle<Puzzle14.Input, Long>(path) {
    override fun parseInput(reader: BufferedReader) = generateSequence { reader.readLine() }
        .toList()
        .toInput()

    data class Rule(val left: Char, val right: Char, val middle: Char) {


        companion object {
            fun String.toRule(): Rule {
                val (before, after) = trim().split(" -> ")
                return Rule(before[0], before[1], after[0])
            }
        }

        override fun toString(): String {
            return "Rule($left$right -> $middle)"
        }
    }

    data class Input(val initial: String, val rules: List<Rule>) {
        companion object {
            fun List<String>.toInput(): Input {
                val rules = drop(2).map { it.toRule() }
                return Input(this[0], rules)
            }
        }
    }

    data class State(val current: Char, val next: Char, val expandCount: Int = 0) {
        override fun toString(): String {
            return "State($current..$next, expanded=$expandCount)"
        }
    }

    override fun solveFirstPart(): Long = countExpanded(10)
    override fun solveSecondPart(): Long = countExpanded(40)

    private fun countExpanded(expandCount: Int): Long {
        val allChars = input.rules.allChars()
        val startingStates = allChars.flatMap { current -> allChars.map { next -> State(current, next) } }
        val endingStates = input.initial.map { it }.windowed(2).map { (left, right) -> State(left, right, expandCount) }

        val stateCounts = Dynamic(
            start = startingStates.map { it to Counter(it.current to 1) },
            categorize = { it.expandCount },
            comparator = naturalOrder(),
            nextStates = input.rules.constructReverseMap(),
            fold = fold(Counter()) { a, b -> a + b },
            filter = { it.expandCount <= expandCount },
        ).compute()

        val expandedCount = endingStates
            .mapNotNull { stateCounts[it] }
            .fold(Counter<Char>()) { a, b -> a + b }
            .apply { add(input.initial.last()) }

        return expandedCount.values.maxOf { it } - expandedCount.values.minOf { it }
    }

    companion object {
        fun List<Rule>.constructReverseMap(): (State) -> Iterable<State> {
            val left = groupBy { it.left to it.middle }.withDefault { listOf() }
            val right = groupBy { it.middle to it.right }.withDefault { listOf() }

            return { state ->
                val key = state.current to state.next

                (left.getValue(key) + right.getValue(key))
                    .map { State(it.left, it.right, state.expandCount + 1) }
            }
        }

        fun List<Rule>.allChars(): Set<Char> = (map { it.left } + map { it.right } + map { it.middle }).toSet()
    }
}

operator fun <T> Counter<T>.plus(other: Counter<T>): Counter<T> = Counter(this).apply {
    other.forEach { value, count -> add(value, count) }
}