package y2021

import BadInput
import Puzzle
import java.io.BufferedReader

class Puzzle3(path: String) : Puzzle<List<String>, Int>(path) {
    override fun parseInput(reader: BufferedReader) = generateSequence { reader.readLine() }.toList()

    override fun solveFirstPart(): Int {
        val sum = input.map { FuzzyBinaryNumber(it) }.reduce { a, b -> a + b }
        return sum.toInt() * (!sum).toInt()
    }

    override fun solveSecondPart(): Int {
        val oxygenGenerator = input.findOxygenGenerator()
        val co2Scrubber = input.findCo2Scrubber()

        return FuzzyBinaryNumber(oxygenGenerator).toInt() * FuzzyBinaryNumber(co2Scrubber).toInt()
    }

    companion object {
        fun List<String>.filterByBitCriteria(currentIndex: Int = 0, bitCriteria: (List<Char>) -> Boolean): String {
            if (size == 1) return this[0]
            if (isEmpty() || currentIndex >= this[0].length) throw BadInput()

            val bits = map { it[currentIndex] }
            val correctBit = if (bitCriteria(bits)) '1' else '0'

            return filter { it[currentIndex] == correctBit }
                .filterByBitCriteria(currentIndex + 1, bitCriteria)
        }

        fun List<String>.findOxygenGenerator(): String =
            filterByBitCriteria {
                it.count { c -> c == '1' } * 2 >= it.size
            }

        fun List<String>.findCo2Scrubber(): String =
            filterByBitCriteria {
                it.count { c -> c == '1' } * 2 < it.size
            }
    }
}

class FuzzyBinaryNumber private constructor(
    private val stats: List<Int>,
    private val count: Int = 1,
) {
    constructor(value: String) : this(fromBinaryToStats(value))

    operator fun plus(other: FuzzyBinaryNumber): FuzzyBinaryNumber {
        if (this.stats.size != other.stats.size) throw BadInput()

        return FuzzyBinaryNumber(
            this.stats.zip(other.stats).map { (a, b) -> a + b },
            this.count + other.count,
        )
    }

    operator fun not(): FuzzyBinaryNumber = FuzzyBinaryNumber(stats.map { -it }, count)

    fun toInt(): Int =
        stats.asReversed()
            .mapIndexed { index, i -> if (i > 0) 1 shl index else 0 }
            .sum()

    override fun toString(): String {
        val stats = stats.joinToString(separator = "") { if (it > 0) "1" else "0" }
        return "0fb$stats"
    }

    companion object {
        private fun fromBinaryToStats(binary: String): List<Int> = binary.map {
            when (it) {
                '1' -> 1
                '0' -> -1
                else -> throw BadInput()
            }
        }
    }
}