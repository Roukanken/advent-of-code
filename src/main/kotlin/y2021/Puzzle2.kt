package y2021

import BadInput
import Puzzle
import y2021.SubmarineCommand.Companion.toSubmarineCommand
import java.io.BufferedReader

class Puzzle2(path: String) : Puzzle<List<SubmarineCommand>, Int>(path) {
    override fun parseInput(reader: BufferedReader) = generateSequence { reader.readLine() }
        .map { it.toSubmarineCommand() }
        .toList()

    override fun solveFirstPart(): Int = Submarine(0, 0).execute(input).coordinatesMultiply()
    override fun solveSecondPart(): Int = Submarine(0, 0).aimedExecute(input).coordinatesMultiply()
}

data class Submarine(val horizontal: Int, val depth: Int, val aim: Int = 0)

fun Submarine.execute(command: SubmarineCommand): Submarine = command.execute(this)
fun Submarine.execute(commands: Iterable<SubmarineCommand>): Submarine =
    commands.fold(this) { submarine, command -> submarine.execute(command) }

fun Submarine.aimedExecute(command: SubmarineCommand): Submarine = command.aimedExecute(this)
fun Submarine.aimedExecute(commands: Iterable<SubmarineCommand>): Submarine =
    commands.fold(this) { submarine, command -> submarine.aimedExecute(command) }

fun Submarine.coordinatesMultiply(): Int = horizontal * depth

sealed class SubmarineCommand {
    abstract fun execute(submarine: Submarine): Submarine
    abstract fun aimedExecute(submarine: Submarine): Submarine

    data class Forward(val amount: Int) : SubmarineCommand() {
        override fun execute(submarine: Submarine): Submarine =
            submarine.run { copy(horizontal = horizontal + amount) }

        override fun aimedExecute(submarine: Submarine): Submarine = submarine.run {
            copy(horizontal = horizontal + amount, depth = depth + aim * amount)
        }
    }

    data class Down(val amount: Int) : SubmarineCommand() {
        override fun execute(submarine: Submarine): Submarine = submarine.run { copy(depth = depth + amount) }
        override fun aimedExecute(submarine: Submarine): Submarine = submarine.run { copy(aim = aim + amount) }
    }

    data class Up(val amount: Int) : SubmarineCommand() {
        override fun execute(submarine: Submarine): Submarine = submarine.run { copy(depth = depth - amount) }
        override fun aimedExecute(submarine: Submarine): Submarine = submarine.run { copy(aim = aim - amount) }
    }

    companion object {
        fun String.toSubmarineCommand(): SubmarineCommand {
            val (name, stringValue) = this.split(" ")
            val value = stringValue.toInt();

            return when (name) {
                "forward" -> Forward(value)
                "down" -> Down(value)
                "up" -> Up(value)
                else -> throw BadInput()
            }
        }
    }
}