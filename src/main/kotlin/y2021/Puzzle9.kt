package y2021

import Location
import Puzzle
import computeReachable
import get
import getOrElse
import getOrNull
import locations
import set
import java.io.BufferedReader

class Puzzle9(path: String) : Puzzle<List<List<Int>>, Int>(path) {
    override fun parseInput(reader: BufferedReader) = generateSequence { reader.readLine() }
        .map { line -> line.trim().map { it - '0' } }
        .toList()

    override fun solveFirstPart(): Int {
        return input.locations()
            .filter { it.fourDirectionNeighborhood().mapNotNull { near -> input.getOrNull(near) }.all { near -> near > input[it] } }
            .sumOf { input[it].plus(1) }
    }

    override fun solveSecondPart(): Int {
        val basins = MutableList(input.size) { MutableList<Location?>(input[0].size) { null } }

        for (location in input.locations()) {
            if (basins[location] != null || !input.isInBasin(location)) continue

            println(location)
            computeReachable(location) { now -> now.fourDirectionNeighborhood().filter { input.isInBasin(it) } }
                .forEach { basins[it] = location }
        }

        val (largest, secondLargest, thirdLargest) = basins.flatten()
            .filterNotNull()
            .groupingBy { it }
            .eachCount()
            .values
            .sortedDescending()

        return largest * secondLargest * thirdLargest
    }

    companion object {
        fun List<List<Int>>.isInBasin(location: Location): Boolean = getOrElse(location) { 9 } < 9
    }
}