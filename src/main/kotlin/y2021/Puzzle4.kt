package y2021

import BadInput
import Puzzle
import transpose
import y2021.BingoBoard.Companion.toBingoBoard
import java.io.BufferedReader

class Puzzle4(path: String) : Puzzle<Puzzle4.Input, Int>(path) {
    data class Input(
        val numbers: LotteryNumbers,
        val boards: List<BingoBoard>,
    ) {
        companion object {
            fun String.toInput(): Input {
                val data = this.trim().split("\n\n")
                val numbers = data[0].trim().split(",").map { it.toInt() }
                val boards = data.drop(1).map { it.trim().toBingoBoard() }

                return Input(LotteryNumbers(numbers), boards)
            }
        }
    }

    override fun parseInput(reader: BufferedReader) = with(Input) { reader.readText().toInput() }

    override fun solveFirstPart(): Int {
        val winningBoard = input.boards
            .minByOrNull { it.getWinTime(input.numbers) ?: Int.MAX_VALUE } ?: throw BadInput()

        return winningBoard.getBoardScore(input.numbers) ?: throw BadInput()
    }

    override fun solveSecondPart(): Int {
        val winningBoard = input.boards
            .maxByOrNull { it.getWinTime(input.numbers) ?: Int.MIN_VALUE } ?: throw BadInput()

        return winningBoard.getBoardScore(input.numbers) ?: throw BadInput()
    }

}

data class LotteryNumbers(val numbers: List<Int>) : List<Int> by numbers {
    private val earliest = numbers.mapIndexed { index, number -> number to index }
        .distinctBy { (number, _) -> number }
        .toMap()

    fun getTimeWhenAllGuessedAreDrawn(guessed: List<Int>): Int? {
        val draws = guessed.mapNotNull { earliest.getOrDefault(it, null) }
        return if (draws.size == guessed.size) draws.maxOrNull() else null
    }
}

class BingoBoard(private val values: List<List<Int>>) {

    private val winningSets: List<List<Int>>
        get() = values + values.transpose()

    fun getWinTime(numbers: LotteryNumbers): Int? = winningSets
        .mapNotNull { numbers.getTimeWhenAllGuessedAreDrawn(it) }
        .minOrNull()

    fun getBoardScore(numbers: LotteryNumbers): Int? {
        val winTime = getWinTime(numbers) ?: return null
        val sumOfUnmarked = getUnmarkedNumbers(numbers.take(winTime + 1)).sum()
        val winningNumber = numbers[winTime]

        return sumOfUnmarked * winningNumber
    }

    private fun getUnmarkedNumbers(marked: Iterable<Int>): List<Int> {
        val markedSet = marked.toSet()

        return this.values
            .flatten()
            .filterNot { it in markedSet }
    }

    companion object {
        fun String.toBingoBoard(): BingoBoard {
            val values = this.trim().split("\n")
                .map { it.trim().split(" +".toRegex()).map { a -> a.toInt() } }

            return BingoBoard(values)
        }
    }

    override fun toString(): String {
        val field = values.joinToString(separator = "\n") {
            " " + it.joinToString(separator = "") { number -> number.toString().padStart(3, ' ') }
        }
        return "BingoBoard(\n$field\n)"
    }
}
