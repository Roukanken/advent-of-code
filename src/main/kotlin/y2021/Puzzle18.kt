package y2021

import BadInput
import Puzzle
import product
import y2021.SnailFishNumber.Companion.toSnailFishNumber
import java.io.BufferedReader

class Puzzle18(path: String) : Puzzle<List<SnailFishNumber>, Int>(path) {
    override fun parseInput(reader: BufferedReader) = generateSequence { reader.readLine() }
        .map { it.toSnailFishNumber() }
        .toList()

    override fun solveFirstPart(): Int = input.reduce { a, b -> a + b }.magnitude()
    override fun solveSecondPart(): Int = (input product input).map { (a, b) -> a + b }.maxOf { it.magnitude() }
}

data class SnailResult<T>(val didSomething: Boolean, val result: SnailFishNumber, val data: T)

sealed class SnailFishNumber {

    fun reduce(): SnailFishNumber {
        val (hadExploded, exploded) = reduceByExplosion()
        if (hadExploded) return exploded.reduce()

        val (hadSplit, splitted) = reduceBySplit()
        if (hadSplit) return splitted.reduce()

        return this
    }

    abstract fun magnitude(): Int

    abstract fun reduceByExplosion(depth: Int = 0): SnailResult<Pair<Int, Int>>
    abstract fun reduceBySplit(): SnailResult<Unit>

    abstract fun addToLeftMost(value: Int): SnailFishNumber
    abstract fun addToRightMost(value: Int): SnailFishNumber

    operator fun plus(other: SnailFishNumber): SnailFishNumber = Complex(this, other).reduce()

    data class Regular(val value: Int) : SnailFishNumber() {
        override fun magnitude(): Int = value

        override fun reduceByExplosion(depth: Int): SnailResult<Pair<Int, Int>> = SnailResult(false, this, 0 to 0)

        override fun reduceBySplit(): SnailResult<Unit> =
            if (value >= 10) SnailResult(true, Complex(Regular(value / 2), Regular(value / 2 + value.rem(2))), Unit)
            else SnailResult(false, this, Unit)

        override fun addToLeftMost(value: Int): SnailFishNumber = Regular(this.value + value)
        override fun addToRightMost(value: Int): SnailFishNumber = Regular(this.value + value)

        override fun toString(): String {
            return "$value"
        }
    }

    data class Complex(val left: SnailFishNumber, val right: SnailFishNumber) : SnailFishNumber() {
        override fun magnitude(): Int = left.magnitude() * 3 + right.magnitude() * 2

        override fun reduceByExplosion(depth: Int): SnailResult<Pair<Int, Int>> {
            if (depth >= 4) {
                check(left is Regular && right is Regular) { "$this should not be able to explode" }
                return SnailResult(true, Regular(0), left.value to right.value)
            }

            val (leftDidSomething, leftResult, leftAdditions) = left.reduceByExplosion(depth + 1)
            if (leftDidSomething) {
                return if (leftAdditions.second > 0) {
                    SnailResult(
                        true,
                        Complex(leftResult, right.addToLeftMost(leftAdditions.second)),
                        leftAdditions.first to 0
                    )
                } else {
                    SnailResult(true, Complex(leftResult, right), leftAdditions)
                }
            }

            val (rightDidSomething, rightResult, rightAdditions) = right.reduceByExplosion(depth + 1)
            if (rightDidSomething) {
                return if (rightAdditions.first > 0) {
                    SnailResult(
                        true,
                        Complex(left.addToRightMost(rightAdditions.first), rightResult),
                        0 to rightAdditions.second
                    )
                } else {
                    SnailResult(true, Complex(left, rightResult), rightAdditions)
                }
            }

            return SnailResult(false, this, 0 to 0)
        }

        override fun reduceBySplit(): SnailResult<Unit> {
            val (leftDidSomething, leftResult) = left.reduceBySplit()
            if (leftDidSomething) return SnailResult(true, Complex(leftResult, right), Unit)

            val (rightDidSomething, rightResult) = right.reduceBySplit()
            if (rightDidSomething) return SnailResult(true, Complex(left, rightResult), Unit)

            return SnailResult(false, this, Unit)
        }

        override fun addToLeftMost(value: Int): SnailFishNumber = Complex(left.addToLeftMost(value), right)
        override fun addToRightMost(value: Int): SnailFishNumber = Complex(left, right.addToRightMost(value))

        override fun toString(): String {
            return "[$left,$right]"
        }
    }

    companion object {
        fun String.toSnailFishNumber(): SnailFishNumber {
            val stack = mutableListOf<SnailFishNumber>()

            for (char in this.trim()) {
                when (char) {
                    in '0'..'9' -> stack.add(Regular(char - '0'))
                    ']' -> {
                        val b = stack.removeLast()
                        val a = stack.removeLast()
                        stack.add(Complex(a, b))
                    }
                    '[', ',' -> Unit
                    else -> throw BadInput()
                }
            }

            return stack.single()
        }
    }
}