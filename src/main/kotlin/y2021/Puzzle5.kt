package y2021

import Location
import Puzzle
import at
import gcd
import y2021.Segment.Companion.toSegment
import java.io.BufferedReader
import kotlin.math.absoluteValue

class Puzzle5(path: String) : Puzzle<List<Segment>, Int>(path) {
    override fun parseInput(reader: BufferedReader) = generateSequence { reader.readLine() }
        .map { it.toSegment() }
        .toList()

    override fun solveFirstPart(): Int {
        return input
            .filter { it.isHorizontal() || it.isVertical() }
            .flatMap { it.points }
            .count()
            .values
            .filter { it >= 2 }
            .size
    }

    override fun solveSecondPart(): Int {
        return input
            .flatMap { it.points }
            .count()
            .values
            .filter { it >= 2 }
            .size
    }
}

data class Segment(val from: Location, val to: Location) {

    fun isHorizontal(): Boolean = from.x == to.x
    fun isVertical(): Boolean = from.y == to.y

    val angle: Location by lazy {
        val delta = to - from
        val gcd = gcd(delta.x.absoluteValue, delta.y.absoluteValue)
        check(gcd != 0) { "Can't determine angle of zero-length segment" }

        Location(delta.x / gcd, delta.y / gcd)
    }

    val points: List<Location> by lazy {
        sequence {
            yield(from)
            yieldAll(from.ray(angle).takeWhile { it != to })
            yield(to)
        }.toList()
    }

    companion object {
        fun String.toSegment(): Segment {
            val (from, to) = trim().split(" -> ")

            return Segment(from.toLocation(), to.toLocation())
        }
    }
}

data class Counter<T>(private val data: MutableMap<T, Long> = mutableMapOf()) : Map<T, Long> by data {
    constructor(vararg data: Pair<T, Long>) : this(data.toMap().toMutableMap())
    constructor(counter: Counter<T>) : this(counter.data.toMutableMap())

    fun add(value: T, count: Long = 1) {
        data[value] = getOrDefault(value, 0) + count
    }

    override fun toString(): String {
        return "Counter$data"
    }
}

fun <T> Counter<T>.addAll(values: Iterable<T>) = values.forEach { add(it) }
fun <T> Iterable<T>.count(): Counter<T> = Counter<T>().also { it.addAll(this) }

fun String.toLocation(delimiters: String = ","): Location {
    val (x, y) = trim().split(delimiters)
    return x.toInt() at y.toInt()
}