package y2021

import BadInput
import Location
import NoIdea
import Puzzle
import at
import y2021.Puzzle17.Input.Companion.toInput
import java.io.BufferedReader
import kotlin.math.sign

class Puzzle17(path: String) : Puzzle<Puzzle17.Input, Int>(path) {
    override fun parseInput(reader: BufferedReader) = reader.readLine().toInput()

    data class Input(val xRange: IntRange, val yRange: IntRange) {
        companion object {
            private val regex = "target area: x=(-?\\d+)..(-?\\d+), y=(-?\\d+)..(-?\\d+)".toRegex()

            fun String.toInput(): Input {
                val (xLow, xHigh, yLow, yHigh) = regex.matchEntire(this.trim())?.destructured ?: throw BadInput()
                return Input(xLow.toInt()..xHigh.toInt(), yLow.toInt()..yHigh.toInt())
            }
        }
    }

    override fun solveFirstPart(): Int = input.findHighestStrategyLetDragTakeCareOfIt()
    override fun solveSecondPart(): Int = input.findAllJustBrute().count()

    companion object {

        fun Input.findHighestStrategyLetDragTakeCareOfIt(): Int {
            val x = hitXAfterDrag(xRange)
            val y = -yRange.first - 1

            val condition = x < y

            return if (condition) (y + 1) * y / 2 else throw NoIdea()
        }

        private fun hitXAfterDrag(xRange: IntRange): Int {
            var x = 0
            var finalX = 0
            while (finalX !in xRange) {
                x += 1
                finalX += x
            }

            return x
        }

        fun Input.findAllJustBrute(): Sequence<Location> = sequence {
            for (x in 0..(xRange.last + 1)) {
                for (y in yRange.first..-yRange.first) {

                    val velocity = x at y
                    if (velocity.simIfHits(xRange, yRange)) yield(velocity)
                }
            }
        }

        private fun Location.simIfHits(xRange: IntRange, yRange: IntRange): Boolean {
            return this.sim()
                .takeWhile { it.y >= yRange.first && it.x <= xRange.last }
                .any { it.y in yRange && it.x in xRange }
        }

        private fun Location.sim(): Sequence<Location> = sequence {
            var velocity = this@sim
            var location = 0 at 0

            while (true) {
                yield(location)
                location += velocity
                velocity -= (velocity.x.sign at 1)
            }
        }
    }
}

