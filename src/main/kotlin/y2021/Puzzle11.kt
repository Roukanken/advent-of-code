package y2021

import Location
import Puzzle
import get
import locations
import set
import java.io.BufferedReader

class Puzzle11(path: String) : Puzzle<List<List<Int>>, Int>(path) {
    override fun parseInput(reader: BufferedReader) = generateSequence { reader.readLine() }
        .map { it.trim().map { c -> c - '0' } }
        .toList()

    override fun solveFirstPart(): Int {
        return input.simulate().take(100).sumOf { it.size }
    }

    override fun solveSecondPart(): Int {
        val totalNumber = input.sumOf { it.size }
        return input.simulate().takeWhile { it.size != totalNumber }.count() + 1
    }

    companion object {
        fun List<List<Int>>.simulate(): Sequence<Set<Location>> = sequence {
            val state = this@simulate.map { it.toMutableList() }.toMutableList()

            while (true) {
                yield(state.simulateStep())
            }
        }

        fun MutableList<MutableList<Int>>.simulateStep(): Set<Location> {
            val flashes = mutableSetOf<Location>()
            val work = locations().toMutableList()

            while (work.isNotEmpty()) {
                val current = work.removeLast()
                if (current in flashes) continue

                this[current] = this[current] + 1
                if (this[current] <= 9) continue

                flashes.add(current)
                current.neighborhood()
                    .filter { it inIndicesOf this }
                    .forEach { work.add(it) }
            }

            flashes.forEach { this[it] = 0 }

            return flashes
        }
    }
}

infix fun <T> Location.inIndicesOf(data: List<List<T>>) = y in data.indices && x in data[y].indices