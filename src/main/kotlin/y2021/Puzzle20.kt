package y2021

import Coordinates
import Puzzle
import y2020.GenericAutomataRule
import y2020.GenericCellAutomata
import java.io.BufferedReader

class Puzzle20(path: String) : Puzzle<List<String>, Int>(path) {
    override fun parseInput(reader: BufferedReader) = generateSequence { reader.readLine() }.toList()

    override fun solveFirstPart(): Int = TODO()
    override fun solveSecondPart(): Int = TODO()

    companion object {}
}

open class FlipFlopCellAutomata<T : Coordinates<T>>(
    override val field: Set<T> = setOf(),
    override val rule: GenericAutomataRule<T>,
    val aliveCells: Boolean = true
) : GenericCellAutomata<T>(field, rule) {

    override fun next(): GenericCellAutomata<T> {
        val activeCells = field.flatMap { it.neighborhood() }.toSet()
        val differentStateFromCurrent = activeCells.filter { cell -> rule(this, cell) != aliveCells }.toSet()
        return FlipFlopCellAutomata(differentStateFromCurrent, rule, !aliveCells)
    }

    override operator fun contains(cell: T): Boolean = if (aliveCells) cell in field else cell !in field
}