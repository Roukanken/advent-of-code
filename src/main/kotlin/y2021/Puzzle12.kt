package y2021

import Dynamic
import Puzzle
import fold
import java.io.BufferedReader

typealias Graph<T> = Map<T, List<T>>

class Puzzle12(path: String) : Puzzle<Graph<Puzzle12.Cave>, Int>(path) {
    override fun parseInput(reader: BufferedReader) = generateSequence { reader.readLine() }
        .map { it.trim().split("-") }
        .map { (a, b) -> Cave(a) to Cave(b) }
        .toList()
        .toGraph()

    @JvmInline
    value class Cave(val name: String) {
        fun isBig(): Boolean = name.all { it.isUpperCase() }
    }

    data class State(val current: Cave, val visitedSmallCaves: Set<Cave> = setOf(current)) {
        fun visit(cave: Cave): State? = if (cave in visitedSmallCaves) null else State(
            cave,
            visitedSmallCaves + if (cave.isBig()) setOf() else setOf(cave),
        )

        fun categorize(): Pair<Int, Boolean> = visitedSmallCaves.size to current.isBig()
    }

    data class RevisitingState(val current: Cave, val revisitedCount: Int = 0, val visitedSmallCaves: Set<Cave> = setOf(current)) {
        fun visit(cave: Cave): RevisitingState? = if (cave.name == "start" || current.name == "end") null else RevisitingState(
            cave,
            revisitedCount + if (cave in visitedSmallCaves) 1 else 0,
            visitedSmallCaves + if (cave.isBig()) setOf() else setOf(cave)
        )

        fun categorize(): Triple<Int, Int, Boolean> = Triple(visitedSmallCaves.size, revisitedCount, current.isBig())
    }

    override fun solveFirstPart(): Int {
        return countAllDifferentPaths(
            start = State(Cave("start")),
            categorize = State::categorize,
            comparator = compareBy<Pair<Int, Boolean>> { it.first }.thenBy { it.second },
            nextStates = { input.getValue(this.current).mapNotNull { this.visit(it) } }
        ).entries
            .filter { it.key.current.name == "end" }
            .sumOf { it.value }
    }

    override fun solveSecondPart(): Int {
        return countAllDifferentPaths(
            start = RevisitingState(Cave("start")),
            categorize = RevisitingState::categorize,
            comparator = compareBy<Triple<Int, Int, Boolean>> { it.first }.thenBy { it.second }.thenBy { it.third },
            nextStates = { input.getValue(this.current).mapNotNull { this.visit(it) }.filter { it.revisitedCount <= 1 } }
        ).entries
            .filter { it.key.current.name == "end" }
            .sumOf { it.value }
    }

    companion object {
        // Expects state.nextStates().all { state.categorize() < it.categorize() } to be always true; otherwise produces invalid results
        fun <State, Category, CategoryComparator : Comparator<Category>> countAllDifferentPaths(
            start: State,
            comparator: CategoryComparator,
            categorize: State.() -> Category,
            nextStates: State.() -> Iterable<State>
        ): Map<State, Int> = Dynamic(
            start = listOf(start to 1),
            categorize = { it.categorize() },
            comparator = comparator,
            nextStates = { it.nextStates() },
            fold = fold(0) { a, b -> a + b },
        ).compute()
    }
}

fun <T> Iterable<Pair<T, T>>.toGraph(): Graph<T> =
    (this + map { it.second to it.first })
        .groupBy({ it.first }, { it.second })
