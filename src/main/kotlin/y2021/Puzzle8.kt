package y2021

import BadInput
import Puzzle
import y2021.Puzzle8.DisplaySegment.Companion.toDisplaySegment
import y2021.Puzzle8.Input.Companion.toInput
import java.io.BufferedReader

val map = mapOf(
    1 to listOf(0, 0, 1, 0, 0, 1, 0),  // 2
    7 to listOf(1, 0, 1, 0, 0, 1, 0),  // 3
    4 to listOf(0, 1, 1, 1, 0, 1, 0),  // 4
    8 to listOf(1, 1, 1, 1, 1, 1, 1),  // 7

    2 to listOf(1, 0, 1, 1, 1, 0, 1),  // 5
    3 to listOf(1, 0, 1, 1, 0, 1, 1),  // 5
    5 to listOf(1, 1, 0, 1, 0, 1, 1),  // 5

    0 to listOf(1, 1, 1, 0, 1, 1, 1),  // 6
    6 to listOf(1, 1, 0, 1, 1, 1, 1),  // 6
    9 to listOf(1, 1, 1, 1, 0, 1, 1),  // 6
)

class Puzzle8(path: String) : Puzzle<List<Puzzle8.Input>, Int>(path) {
    override fun parseInput(reader: BufferedReader) = generateSequence { reader.readLine() }
        .map { it.toInput() }
        .toList()

    enum class DisplaySegment {
        A, B, C, D, E, F, G;

        companion object {
            val all = setOf(A, B, C, D, E, F, G)

            fun Char.toDisplaySegment(): DisplaySegment = when (this) {
                'a' -> A
                'b' -> B
                'c' -> C
                'd' -> D
                'e' -> E
                'f' -> F
                'g' -> G
                else -> throw BadInput()
            }

            val digits = mapOf(
                setOf(A, B, C, E, F, G) to 0,
                setOf(C, F) to 1,
                setOf(A, C, D, E, G) to 2,
                setOf(A, C, D, F, G) to 3,
                setOf(B, C, D, F) to 4,
                setOf(A, B, D, F, G) to 5,
                setOf(A, B, D, E, F, G) to 6,
                setOf(A, C, F) to 7,
                setOf(A, B, C, D, E, F, G) to 8,
                setOf(A, B, C, D, F, G) to 9,
            )
        }
    }

    data class Input(val definitions: List<Set<DisplaySegment>>, val displayed: List<Set<DisplaySegment>>) {

        val reverseMapping: Map<DisplaySegment, DisplaySegment> by lazy { definitions.identifyReverseMapping() }

        val displayedDigits: List<Int> by lazy {
            displayed.map { digit -> digit.mapNotNull { reverseMapping[it] }.toSet().toDigit() }
        }

        companion object {
            fun String.toInput(): Input {
                val (digits, displayed) = trim().split(" | ")
                return Input(digits.toDigits(), displayed.toDigits())
            }

            private fun String.toDigits(): List<Set<DisplaySegment>> = trim().split(" ").map { it.toDigit() }
            private fun String.toDigit(): Set<DisplaySegment> = map { it.toDisplaySegment() }.toSet()
        }
    }


    override fun solveFirstPart(): Int {
        return input.map { it.displayedDigits }
            .sumOf { it.count { digit -> digit in setOf(1, 4, 7, 8) } }
    }

    override fun solveSecondPart(): Int {
        return input.map { it.displayedDigits }
            .sumOf { it.reduce { number, digit -> number * 10 + digit } }
    }

    companion object {
        /**
         * len 2 -> x in { c, f }
         * len 3 + len 2 -> x == a
         * len 2 + len 4 -> x in { b, d }
         * len 6 -> x in { c, d, e }
         *
         * x in { c, d, e } + x in { c, g } -> x == c, x == g
         * x in { c, d, e } + x == c + x in { b, d } -> x == b, x == d, x == e
         *
         * left -> g
         */

        fun List<Set<DisplaySegment>>.identifyReverseMapping(): Map<DisplaySegment, DisplaySegment> {
            val digitsBySize = groupBy { it.size }

            val cf = digitsBySize[2]?.singleOrNull() ?: throw BadInput()
            val acf = digitsBySize[3]?.singleOrNull() ?: throw BadInput()
            val bcdf = digitsBySize[4]?.singleOrNull() ?: throw BadInput()

            val cde = (digitsBySize[6] ?: throw BadInput())
                .map { DisplaySegment.all - it }
                .reduce { a, b -> a + b }

            val bd = bcdf - cf
            val a = acf - cf
            val b = bd - cde
            val f = cf - cde
            val c = cf - f
            val e = cde - c - bd
            val d = cde - c - e
            val g = DisplaySegment.all - a - bcdf - e

            return mapOf(
                a.single() to DisplaySegment.A,
                b.single() to DisplaySegment.B,
                c.single() to DisplaySegment.C,
                d.single() to DisplaySegment.D,
                e.single() to DisplaySegment.E,
                f.single() to DisplaySegment.F,
                g.single() to DisplaySegment.G,
            )
        }

        fun Set<DisplaySegment>.toDigit(): Int = DisplaySegment.digits[this] ?: throw BadInput()
    }
}
