package y2021

import BadInput
import Puzzle
import arrow.core.Either
import y2021.Puzzle10.Parenthesis.Companion.toParenthesis
import java.io.BufferedReader

class Puzzle10(path: String) : Puzzle<List<String>, Long>(path) {
    override fun parseInput(reader: BufferedReader) = generateSequence { reader.readLine() }.toList()

    enum class Parenthesis {
        OPENING_NORMAL, CLOSING_NORMAL,
        OPENING_SQUARE, CLOSING_SQUARE,
        OPENING_SQUIRLY, CLOSING_SQUIRLY,
        OPENING_ANGLED, CLOSING_ANGLED,
        ;

        fun isOpening(): Boolean = this in openingBrackets
        fun isClosing(): Boolean = !isOpening()

        fun matches(other: Parenthesis): Boolean = setOf(this, other) in bracketSets

        fun illegalPoints(): Long = when (this) {
            OPENING_NORMAL -> 3
            CLOSING_NORMAL -> 3
            OPENING_SQUARE -> 57
            CLOSING_SQUARE -> 57
            OPENING_SQUIRLY -> 1197
            CLOSING_SQUIRLY -> 1197
            OPENING_ANGLED -> 25137
            CLOSING_ANGLED -> 25137
        }

        fun autocompletePoints(): Long = when (this) {
            OPENING_NORMAL -> 1
            CLOSING_NORMAL -> 1
            OPENING_SQUARE -> 2
            CLOSING_SQUARE -> 2
            OPENING_SQUIRLY -> 3
            CLOSING_SQUIRLY -> 3
            OPENING_ANGLED -> 4
            CLOSING_ANGLED -> 4
        }

        companion object {
            private val openingBrackets = setOf(OPENING_NORMAL, OPENING_SQUARE, OPENING_SQUIRLY, OPENING_ANGLED)
            private val bracketSets = setOf(
                setOf(OPENING_NORMAL, CLOSING_NORMAL),
                setOf(OPENING_SQUARE, CLOSING_SQUARE),
                setOf(OPENING_SQUIRLY, CLOSING_SQUIRLY),
                setOf(OPENING_ANGLED, CLOSING_ANGLED),
            )

            fun Char.toParenthesis(): Parenthesis = when (this) {
                '(' -> OPENING_NORMAL
                ')' -> CLOSING_NORMAL
                '[' -> OPENING_SQUARE
                ']' -> CLOSING_SQUARE
                '{' -> OPENING_SQUIRLY
                '}' -> CLOSING_SQUIRLY
                '<' -> OPENING_ANGLED
                '>' -> CLOSING_ANGLED
                else -> throw BadInput()
            }
        }
    }

    override fun solveFirstPart(): Long {
        return input.map { it.validateParenthesis() }
            .filterIsInstance<Either.Left<Parenthesis>>()
            .sumOf { it.value.illegalPoints() }
    }

    override fun solveSecondPart(): Long {
        return input.map { it.validateParenthesis() }
            .filterIsInstance<Either.Right<List<Parenthesis>>>()
            .map { it.value.foldRight(0L) { parenthesis, acc -> acc * 5 + parenthesis.autocompletePoints() } }
            .median()
    }

    companion object {
        fun String.validateParenthesis(): Either<Parenthesis, List<Parenthesis>> {
            val stack = mutableListOf<Parenthesis>()

            for (char in this) {
                val parenthesis = char.toParenthesis()

                if (parenthesis.isOpening()) {
                    stack.add(parenthesis)
                } else {
                    val last = stack.removeLast()
                    if (!last.matches(parenthesis)) return Either.Left(parenthesis)
                }
            }

            return Either.Right(stack)
        }
    }
}