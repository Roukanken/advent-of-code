package y2021

import BadInput
import Puzzle
import at
import get
import hasIndex
import java.io.BufferedReader
import java.util.*
import kotlin.collections.set

class Puzzle15(path: String) : Puzzle<List<List<Int>>, Int>(path) {
    override fun parseInput(reader: BufferedReader) = generateSequence { reader.readLine() }
        .map { line -> line.trim().map { it - '0' } }
        .toList()


    override fun solveFirstPart(): Int = input.shortestFromCornerToCorner() ?: throw BadInput()
    override fun solveSecondPart(): Int = input
        .tileAccumulating(width = 5, height = 5) { if (it == 9) 1 else (it + 1) }
        .shortestFromCornerToCorner() ?: throw BadInput()

    companion object {
        fun List<List<Int>>.shortestFromCornerToCorner() = dijkstraShortestPath(0 at 0) { now ->
            now.fourDirectionNeighborhood().filter { this hasIndex it }.map { it to this[it] }
        }[this[0].size - 1 at this.size - 1]

        fun <T> dijkstraShortestPath(start: T, getNeighborhood: (T) -> Iterable<Pair<T, Int>>): Map<T, Int> {
            val reached = mutableMapOf<T, Int>()
            val work = PriorityQueue<Pair<T, Int>>(compareBy { it.second }).apply { add(start to 0) }

            while (work.isNotEmpty()) {
                val (current, value) = work.remove()
                if (current in reached) continue

                reached[current] = value

                getNeighborhood(current).forEach { (next, nextCost) ->
                    work.add(next to (value + nextCost))
                }
            }

            return reached
        }
    }
}

private fun <T> List<List<T>>.tileAccumulating(width: Int, height: Int, transformation: (T) -> T): List<List<T>> {
    val extended = map { line ->
        (0 until width - 1).scan(line) { it, _ -> it.map(transformation) }.flatten()
    }

    return (0 until height - 1).scan(extended) { tileRow, _ -> tileRow.map { it.map(transformation) } }.flatten()
}

