package y2021

import Puzzle
import java.io.BufferedReader

class Puzzle1(path: String) : Puzzle<List<Int>, Int>(path) {
    override fun parseInput(reader: BufferedReader) = generateSequence { reader.readLine()?.toInt() }
        .toList()

    override fun solveFirstPart(): Int = input.countIncreases()
    override fun solveSecondPart(): Int = input.countWindowedIncreases(3)

    companion object {
        fun List<Int>.countIncreases(): Int =
            windowed(2).count { (last, now) -> last < now }

        fun List<Int>.countWindowedIncreases(size: Int): Int =
            windowed(size)
                .map { it.sum() }
                .countIncreases()
    }
}

class Puzzle1Alternative(path: String) : Puzzle<List<Int>, Int>(path) {
    override fun parseInput(reader: BufferedReader) = generateSequence { reader.readLine()?.toInt() }
        .toList()

    override fun solveFirstPart(): Int = input.countIncreasesOfSums(1)
    override fun solveSecondPart(): Int = input.countIncreasesOfSums(3)

    companion object {
        fun List<Int>.countIncreasesOfSums(size: Int): Int =
            zip(this.drop(size))
                .count { (last, now) -> last < now }
    }
}

