package y2021

import BadInput
import ComplicatedPuzzle
import Location
import UnreachableCode
import at
import y2021.Puzzle13.Fold.Companion.toFold
import y2021.Puzzle13.Input.Companion.toInput
import java.io.BufferedReader
import kotlin.math.absoluteValue

class Puzzle13(path: String) : ComplicatedPuzzle<Puzzle13.Input, Int, String>(path) {
    override fun parseInput(reader: BufferedReader) = generateSequence { reader.readLine() }.toList().toInput()

    data class Input(val locations: List<Location>, val folds: List<Fold>) {
        companion object {
            fun List<String>.toInput(): Input {
                val locations = takeWhile { it.isNotBlank() }.map { it.toLocation() }
                val folds = dropWhile { it.isNotBlank() }.drop(1).map { it.toFold() }

                return Input(locations, folds)
            }
        }
    }

    sealed class Fold {
        abstract fun fold(location: Location): Location

        class AlongX(val x: Int) : Fold() {
            override fun fold(location: Location): Location = location.copy(x = location.x.foldBy(this.x))
        }

        class AlongY(val y: Int) : Fold() {
            override fun fold(location: Location): Location = location.copy(y = location.y.foldBy(this.y))
        }

        companion object {
            fun Int.foldBy(foldBy: Int): Int = foldBy - (this - foldBy).absoluteValue

            private val foldRegex = "fold along ([xy])=(\\d+)".toRegex()
            fun String.toFold(): Fold {
                val (axis, value) = foldRegex.matchEntire(this)?.destructured ?: throw BadInput()

                return when (axis) {
                    "x" -> AlongX(value.toInt())
                    "y" -> AlongY(value.toInt())
                    else -> throw UnreachableCode()
                }
            }
        }
    }

    override fun solveFirstPart(): Int {
        return input.locations.map { it.foldBy(input.folds[0]) }
            .toSet()
            .size
    }

    override fun solveSecondPart(): String {
        return input.locations.map { it.foldByAll(input.folds) }
            .toSet()
            .renderAscii()
    }

    companion object {
        fun Location.foldBy(fold: Fold): Location = fold.fold(this)
        fun Location.foldByAll(folds: Iterable<Fold>): Location = folds.fold(this) { current, fold -> current.foldBy(fold) }
    }
}

fun Collection<Location>.renderAscii(presentChar: Char = '█', spaceChar: Char = ' '): String {
    val max = maxOf { it.x } at maxOf { it.y }
    val data = MutableList(max.y + 1) { MutableList(max.x + 1) { spaceChar } }

    forEach { data[it.y][it.x] = presentChar }

    return data.joinToString(separator = "\n") { it.joinToString(separator = "") }
}
