package y2021

import Puzzle
import UnreachableCode
import java.io.BufferedReader
import java.io.EOFException

class Puzzle16(path: String) : Puzzle<BitsProtocol, Long>(path) {
    override fun parseInput(reader: BufferedReader) = BitsProtocol.read(HexadecimalReader(reader.readLine().trim()))

    override fun solveFirstPart(): Long = input.sumVersions()
    override fun solveSecondPart(): Long = input.evaluate()

    companion object {
        fun BitsProtocol.sumVersions() = recursiveFold({ it.version.toLong() }) { acc, it -> acc.sum() + it.version }
        fun BitsProtocol.evaluate() = recursiveFold({ it.value }) { acc, it -> it.operation(acc) }
    }
}

fun <T> BitsProtocol.recursiveFold(initial: (BitsProtocol.Literal) -> T, fold: (acc: List<T>, it: BitsProtocol.Operation) -> T): T =
    when (this) {
        is BitsProtocol.Literal -> initial(this)
        is BitsProtocol.Operation -> fold(this.packets.map { it.recursiveFold(initial, fold) }, this)
    }


enum class BitsOperation(val operation: (List<Long>) -> Long) {
    SUM({ it.sum() }),
    PRODUCT({ it.fold(1) { acc, i -> acc * i } }),
    MINIMUM({ it.minOf { a -> a } }),
    MAXIMUM({ it.maxOf { a -> a } }),
    LITERAL({ TODO() }),  // special case
    GREATER_THAN({ if (it[0] > it[1]) 1 else 0 }),
    LESS_THAN({ if (it[0] < it[1]) 1 else 0 }),
    EQUAL_TO({ if (it[0] == it[1]) 1 else 0 }),
    ;

    operator fun invoke(data: List<Long>): Long = operation(data)

    companion object {
        fun read(reader: HexadecimalReader): BitsOperation = when (reader.readInt(3)) {
            0 -> SUM
            1 -> PRODUCT
            2 -> MINIMUM
            3 -> MAXIMUM
            4 -> LITERAL
            5 -> GREATER_THAN
            6 -> LESS_THAN
            7 -> EQUAL_TO
            else -> throw UnreachableCode()
        }
    }
}

sealed class BitsProtocol(val version: Int, val length: Int) {

    class Literal(version: Int, length: Int, val value: Long) : BitsProtocol(version, length) {
        companion object {
            fun read(version: Int, reader: HexadecimalReader): Literal {
                val bytes = sequence {
                    while (reader.readBoolean())
                        yield(reader.readInt(4))

                    yield(reader.readInt(4))
                }.toList()

                val value = bytes.fold(0L) { acc, byte -> (acc shl 4) + byte }
                return Literal(version, bytes.size * 5 + 6, value)
            }
        }
    }

    class Operation(version: Int, length: Int, val operation: BitsOperation, val packets: List<BitsProtocol>) : BitsProtocol(version, length) {
        companion object {
            fun read(version: Int, operation: BitsOperation, reader: HexadecimalReader): Operation {
                return when (reader.readBoolean()) {
                    false -> readByTotalLength(version, operation, reader)
                    true -> readByPacketCount(version, operation, reader)
                }
            }

            private fun readByTotalLength(version: Int, operation: BitsOperation, reader: HexadecimalReader): Operation {
                val totalPacketsLength = reader.readInt(15)
                val packets = sequence {
                    var packetsLength = 0;

                    while (packetsLength < totalPacketsLength) {
                        val packet = BitsProtocol.read(reader)
                        yield(packet)
                        packetsLength += packet.length
                    }
                }.toList()

                return Operation(version, 6 + 1 + 15 + totalPacketsLength, operation, packets)
            }

            private fun readByPacketCount(version: Int, operation: BitsOperation, reader: HexadecimalReader): Operation {
                val count = reader.readInt(11)
                val packets = (0 until count).map { BitsProtocol.read(reader) }

                val length = 6 + 1 + 11 + packets.sumOf { it.length }

                return Operation(version, length, operation, packets)
            }

        }
    }

    companion object {
        fun read(reader: HexadecimalReader): BitsProtocol {
            val version = reader.readInt(3)
            val operation = BitsOperation.read(reader)

            return when (operation) {
                BitsOperation.LITERAL -> Literal.read(version, reader)
                else -> Operation.read(version, operation, reader)
            }
        }
    }
}


class HexadecimalReader(val data: String) {
    private var position = 0
    private var subPosition = 0

    init {
        require(data.all { it in hexadecimalDigits })
    }

    fun readBits(count: Int): String {
        val startPosition = position;
        val startSubPosition = subPosition;

        subPosition = (startSubPosition + count) % 4
        position += count / 4 + if (subPosition < startSubPosition) 1 else 0

        if (position > data.length || position == data.length && subPosition > 0)
            throw EOFException()

        if (startPosition == position) return getBinaryChunk(position).substring(startSubPosition until subPosition)

        val start = getBinaryChunk(startPosition).substring(startSubPosition until 4)
        val middle = (startPosition + 1 until position).joinToString(separator = "") { getBinaryChunk(it) }
        val end = if (subPosition == 0) "" else getBinaryChunk(position).substring(0 until subPosition)

        return start + middle + end
    }

    fun readInt(count: Int): Int = readBits(count).toInt(radix = 2)
    fun readBoolean(): Boolean = readBits(1) == "1"

    private fun getBinaryChunk(position: Int) = hexadecimalDigits.getValue(data[position])

    companion object {
        private val hexadecimalDigits = mapOf(
            '0' to "0000",
            '1' to "0001",
            '2' to "0010",
            '3' to "0011",
            '4' to "0100",
            '5' to "0101",
            '6' to "0110",
            '7' to "0111",
            '8' to "1000",
            '9' to "1001",
            'A' to "1010",
            'B' to "1011",
            'C' to "1100",
            'D' to "1101",
            'E' to "1110",
            'F' to "1111",
        )
    }
}