package y2021

import Puzzle
import java.io.BufferedReader

class Puzzle6(path: String) : Puzzle<List<Fish>, Long>(path) {
    override fun parseInput(reader: BufferedReader) = reader.readLine()
        .trim()
        .split(",")
        .map { Fish(it.toInt()) }

    override fun solveFirstPart(): Long {
        return input.count()
            .simulateAll()
            .take(80)
            .last()
            .values
            .sum()
    }

    override fun solveSecondPart(): Long {
        return input.count()
            .simulateAll()
            .take(256)
            .last()
            .values
            .sum()
    }

    companion object {
        fun Map<Fish, Long>.simulate(): Map<Fish, Long> = mutableMapOf<Fish, Long>().also { after ->
            entries.flatMap { (fish, count) -> fish.simulate().map { it to count } }
                .forEach { (fish, count) -> after[fish] = after.getOrDefault(fish, 0) + count }
        }

        fun Map<Fish, Long>.simulateAll(): Sequence<Map<Fish, Long>> = generateSequence(this) { it.simulate() }.drop(1)
    }
}

data class Fish(val daysLeft: Int) {
    fun simulate(): List<Fish> = if (daysLeft != 0) {
        listOf(Fish(daysLeft - 1))
    } else {
        listOf(Fish(6), Fish(8))
    }
}