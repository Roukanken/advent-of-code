package y2022

import Puzzle
import java.io.BufferedReader

class Puzzle3(path: String) : Puzzle<List<String>, Int>(path) {
    override fun parseInput(reader: BufferedReader) = generateSequence { reader.readLine() }.toList()

    override fun solveFirstPart(): Int = input
        .asSequence()
        .map {
            val half = it.length / 2
            it.slice(0..<half) to it.slice(half..<it.length)
        }
        .map { (left, right) -> left.toSet() intersect right.toSet() }
        .map { intersection -> intersection.single().itemValue() }
        .sum()

    override fun solveSecondPart(): Int = input
        .asSequence()
        .chunked(3)
        .map { elves -> elves.map { it.toSet() }.reduce { a, b -> a intersect b } }
        .map { intersection -> intersection.single().itemValue() }
        .sum()

    companion object {
        private val itemTypes = ('a'..'z').joinToString(separator = "") + ('A'..'Z').joinToString(separator = "")
        fun Char.itemValue(): Int = itemTypes.indexOf(this) + 1
    }
}