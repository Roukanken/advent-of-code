package y2022

import Puzzle
import transpose
import java.io.BufferedReader

data class Shipyard(val stacks: List<Stack>) {
    data class Stack(var crates: List<Crate>) {
        fun copy() = Stack(crates)
    }

    @JvmInline
    value class Crate(val label: Char)

    fun copy() = Shipyard(stacks.map { it.copy() })

    fun execute(instruction: CraneInstruction, sticky: Boolean = false): Shipyard {
        val source = stacks[instruction.source]
        val target = stacks[instruction.target]

        val cratesToMove = source.crates.takeLast(instruction.count)
        target.crates += if (sticky) cratesToMove else cratesToMove.reversed()
        source.crates = source.crates.dropLast(instruction.count)

        return this
    }

    val visibleCrates: List<Crate>
        get() = stacks.map { it.crates.last() }
}

private fun String.toShipyard(): Shipyard {
    val lines = split("\n")
    val stackLines = lines.dropLast(1).map { it.toList() }.transpose(fill = ' ').map { it.reversed() }

    val stacks = stackLines
        .map { stack -> stack.filterNot { it in listOf(' ', '[', ']') } }
        .filter { it.isNotEmpty() }
        .map { stack -> Shipyard.Stack(stack.map { Shipyard.Crate(it) }.toMutableList()) }

    return Shipyard(stacks = stacks)
}

data class CraneInstruction(val count: Int, val source: Int, val target: Int)

private fun String.toCraneInstruction(): CraneInstruction {
    val matchGroup = "move (\\d+) from (\\d+) to (\\d+)".toRegex().matchEntire(this)
    check(matchGroup != null) { "Crane instruction '$this' is in incorrect format" }

    val (count, source, target) = matchGroup.groupValues.drop(1).map { it.toInt() }
    return CraneInstruction(count, source - 1, target - 1)
}

class Puzzle5(path: String) : Puzzle<Puzzle5.Input, String>(path) {
    data class Input(val shipyard: Shipyard, val craneInstructions: List<CraneInstruction>)

    override fun parseInput(reader: BufferedReader) = reader.readText().parseInput()

    override fun solveFirstPart(): String = input.craneInstructions
        .fold(input.shipyard.copy()) { ship, instruction -> ship.execute(instruction) }
        .visibleCrates
        .toOutput()

    override fun solveSecondPart(): String = input.craneInstructions
        .fold(input.shipyard.copy()) { ship, instruction -> ship.execute(instruction, sticky = true) }
        .visibleCrates
        .toOutput()

    companion object {
        fun String.parseInput(): Input {
            val (shipyard, instructions) = this.split("\n\n")
            return Input(shipyard.toShipyard(), instructions.trim().split("\n").map { it.toCraneInstruction() })
        }

        fun List<Shipyard.Crate>.toOutput(): String = map { it.label }.joinToString(separator = "")
    }
}
