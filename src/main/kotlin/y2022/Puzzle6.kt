package y2022

import Puzzle
import java.io.BufferedReader

class Puzzle6(path: String) : Puzzle<String, Int>(path) {
    override fun parseInput(reader: BufferedReader) = reader.readLine()

    override fun solveFirstPart(): Int = input.findFirstSequenceOfNDistinctCharacters(4)

    override fun solveSecondPart(): Int = input.findFirstSequenceOfNDistinctCharacters(14)

    companion object {
        private fun String.findFirstSequenceOfNDistinctCharacters(n: Int) =
            windowed(n).indexOfFirst { it.toSet().size == n } + n
    }
}
