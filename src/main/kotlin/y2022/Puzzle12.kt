package y2022

import BadInput
import Location
import Puzzle
import SolutionNotFound
import at
import get
import hasIndex
import java.io.BufferedReader
import java.util.*

data class DijkstraEntry<State, Distance : Comparable<Distance>>(val state: State, val distance: Distance)

fun <State, Distance : Comparable<Distance>> dijkstraSearch(
    start: DijkstraEntry<State, Distance>,
    neighborhood: (DijkstraEntry<State, Distance>) -> Iterable<DijkstraEntry<State, Distance>>,
): Sequence<DijkstraEntry<State, Distance>> = sequence {
    val queue = PriorityQueue<DijkstraEntry<State, Distance>>(compareBy { it.distance }).apply { add(start) }
    val visited = mutableSetOf<State>()

    while (queue.isNotEmpty()) {
        val current = queue.remove()

        if (current.state !in visited) {
            yield(current)
            visited.add(current.state)

            queue.addAll(neighborhood(current))
        }
    }
}

fun <State, Distance : Comparable<Distance>> ((State) -> Iterable<DijkstraEntry<State, Distance>>).additiveize(plus: (Distance, Distance) -> Distance)
        : (DijkstraEntry<State, Distance>) -> Iterable<DijkstraEntry<State, Distance>> = { param ->
    this(param.state).map { DijkstraEntry(it.state, plus(param.distance, it.distance)) }
}

fun <State> breadthFirstSearch(start: State, neighborhood: (State) -> Iterable<State>) = dijkstraSearch(
    start = DijkstraEntry(start, 0),
    neighborhood = { state: State -> neighborhood(state).map { DijkstraEntry(it, 1) } }.additiveize { a, b -> a + b }
)

class Puzzle12(path: String) : Puzzle<Puzzle12.Input, Int>(path) {
    data class Input(
        val start: Location,
        val end: Location,
        val map: List<List<Int>>,
    )

    override fun parseInput(reader: BufferedReader) = generateSequence { reader.readLine() }.toInput()

    override fun solveFirstPart(): Int = breadthFirstSearch(input.start) { position ->
        position.fourDirectionNeighborhood()
            .filter { input.map hasIndex it }
            .filter { input.map[it] <= input.map[position] + 1 }
    }
        .find { entry -> entry.state == input.end }
        ?.distance ?: throw SolutionNotFound()

    override fun solveSecondPart(): Int = breadthFirstSearch(input.end) { position ->
        position.fourDirectionNeighborhood()
            .filter { input.map hasIndex it }
            .filter { input.map[it] + 1 >= input.map[position] }
    }
        .filter { input.map[it.state] == 0 }
        .minOf { it.distance }

    companion object {
        fun Sequence<String>.toInput(): Input {
            var start: Location? = null
            var end: Location? = null

            val map = mapIndexed { x, line ->
                line.mapIndexed { y, char ->
                    when (char) {
                        'S' -> {
                            start = x at y
                            'a'
                        }

                        'E' -> {
                            end = x at y
                            'z'
                        }

                        in 'a'..'z' -> char
                        else -> throw BadInput()
                    } - 'a'
                }
            }.toList()

            check(start != null && end != null)
            return Input(start = start!!, end = end!!, map = map)
        }
    }
}