package y2022

import Puzzle
import transpose
import java.io.BufferedReader

class Monkey(
    val items: MutableList<Long>,
    val operation: (Long) -> Long,
    val testDivisibleBy: Long,
    var targetIfTrue: Monkey?,
    var targetIfFalse: Monkey?
)

class InputBuilder(val monkeys: MutableMap<Long, MonkeyBuilder> = mutableMapOf()) {
    class MonkeyBuilder(
        val number: Long,
        private var items: List<Long>? = null,
        private var operation: ((Long) -> Long)? = null,
        private var testDivisibleBy: Long? = null,
        var targetIfFalse: MonkeyBuilder? = null,
        var targetIfTrue: MonkeyBuilder? = null
    ) {
        fun items(vararg items: Long) {
            this.items = items.toList()
        }

        fun operation(operation: ((Long) -> Long)) {
            this.operation = operation
        }

        fun testDivisibleBy(test: Long) {
            this.testDivisibleBy = test
        }

        fun targetIfFalse(targetIfFalse: MonkeyBuilder) {
            this.targetIfFalse = targetIfFalse
        }

        fun targetIfTrue(targetIfTrue: MonkeyBuilder) {
            this.targetIfTrue = targetIfTrue
        }

        fun build(): Monkey {
            require(items != null)
            require(operation != null)
            require(testDivisibleBy != null)
            require(targetIfFalse != null)
            require(targetIfTrue != null)

            return Monkey(
                items = items!!.toMutableList(),
                operation = operation!!,
                testDivisibleBy = testDivisibleBy!!,
                targetIfFalse = null,
                targetIfTrue = null
            )
        }
    }

    fun monkey(number: Long, block: MonkeyBuilder.() -> Unit = { }): MonkeyBuilder = monkeys.getOrPut(number) { MonkeyBuilder(number) }.apply(block)

    fun build(): List<Monkey> {
        val builtMonkeys = monkeys.mapValues { (_, value) -> value.build() }
        builtMonkeys.forEach { (key, monkey) ->
            monkey.targetIfTrue = builtMonkeys[monkeys[key]!!.targetIfTrue!!.number]!!
            monkey.targetIfFalse = builtMonkeys[monkeys[key]!!.targetIfFalse!!.number]!!
        }

        return builtMonkeys.entries.sortedBy { it.key }.map { it.value }
    }
}

fun monkeys(block: InputBuilder.() -> Unit): InputBuilder = InputBuilder().apply(block)

class Puzzle11(path: String) : Puzzle<InputBuilder, Long>(path) {
    override fun parseInput(reader: BufferedReader) = monkeys {
        monkey(0) {
            items(66, 59, 64, 51)
            operation { it * 3 }
            testDivisibleBy(2)
            targetIfTrue(monkey(1))
            targetIfFalse(monkey(4))
        }

        monkey(1) {
            items(67, 61)
            operation { it * 19 }
            testDivisibleBy(7)
            targetIfTrue(monkey(3))
            targetIfFalse(monkey(5))
        }

        monkey(2) {
            items(86, 93, 80, 70, 71, 81, 56)
            operation { it + 2 }
            testDivisibleBy(11)
            targetIfTrue(monkey(4))
            targetIfFalse(monkey(0))
        }

        monkey(3) {
            items(94)
            operation { it * it }
            testDivisibleBy(19)
            targetIfTrue(monkey(7))
            targetIfFalse(monkey(6))
        }

        monkey(4) {
            items(71, 92, 64)
            operation { it + 8 }
            testDivisibleBy(3)
            targetIfTrue(monkey(5))
            targetIfFalse(monkey(1))
        }

        monkey(5) {
            items(58, 81, 92, 75, 56)
            operation { it + 6 }
            testDivisibleBy(5)
            targetIfTrue(monkey(3))
            targetIfFalse(monkey(6))
        }

        monkey(6) {
            items(82, 98, 77, 94, 86, 81)
            operation { it + 7 }
            testDivisibleBy(17)
            targetIfTrue(monkey(7))
            targetIfFalse(monkey(2))
        }

        monkey(7) {
            items(54, 95, 70, 93, 88, 93, 63, 50)
            operation { it + 4 }
            testDivisibleBy(13)
            targetIfTrue(monkey(2))
            targetIfFalse(monkey(0))
        }
    }

    val sampleInput = monkeys {
        monkey(0) {
            items(79, 98)
            operation { it * 19 }
            testDivisibleBy(23)
            targetIfTrue(monkey(2))
            targetIfFalse(monkey(3))
        }

        monkey(1) {
            items(54, 65, 75, 74)
            operation { it + 6 }
            testDivisibleBy(19)
            targetIfTrue(monkey(2))
            targetIfFalse(monkey(0))
        }

        monkey(2) {
            items(79, 60, 97)
            operation { it * it }
            testDivisibleBy(13)
            targetIfTrue(monkey(1))
            targetIfFalse(monkey(3))
        }

        monkey(3) {
            items(74)
            operation { it + 3 }
            testDivisibleBy(17)
            targetIfTrue(monkey(0))
            targetIfFalse(monkey(1))
        }
    }

    override fun solveFirstPart(): Long =
        input.build().simulate(rounds = 20) { it / 3 }
            .transpose()
            .map { it.sum() }
            .sortedDescending()
            .take(2)
            .reduce { a, b -> a * b }

    val gcd = input.build().map { it.testDivisibleBy }.reduce { a, b -> a * b }

    override fun solveSecondPart(): Long =
        input.build().simulate(rounds = 10000) { it % gcd }
            .transpose()
            .map { it.sum() }
            .sortedDescending()
            .take(2)
            .reduce { a, b -> a * b }

    companion object {
        fun Monkey.turn(normalize: (Long) -> Long): Long = items.size.toLong().also {
            for (item in items) {
                val newItem = normalize(operation(item))
                val target = if (newItem % testDivisibleBy == 0L) targetIfTrue else targetIfFalse
                target!!.items.add(newItem)
            }

            items.clear()
        }

        fun List<Monkey>.round(normalize: (Long) -> Long): List<Long> = map { it.turn(normalize) }

        fun List<Monkey>.simulate(rounds: Long, normalize: (Long) -> Long) = (0..<rounds).map { round(normalize) }

        fun List<Monkey>.debug() = forEachIndexed { number, monkey ->
            println("Monkey $number: ${monkey.items})")
        }.also { println() }
    }
}
