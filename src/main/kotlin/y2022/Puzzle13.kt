package y2022

import Puzzle
import arrow.core.compareTo
import sep
import java.io.BufferedReader

class Puzzle13(path: String) : Puzzle<List<Pair<Puzzle13.Entry.Multiple, Puzzle13.Entry.Multiple>>, Int>(path) {
    sealed interface Entry : Comparable<Entry> {
        data class Single(val value: Int) : Entry {
            override fun compareTo(other: Entry): Int =
                when (other) {
                    is Multiple -> this.wrap().compareTo(other)
                    is Single -> this.value.compareTo(other.value)
                }

            fun wrap(): Multiple = Multiple(listOf(this))
        }

        data class Multiple(val values: List<Entry>) : Entry {
            override fun compareTo(other: Entry): Int =
                when (other) {
                    is Single -> this.compareTo(other.wrap())
                    is Multiple -> this.values.compareTo(other.values)
                }
        }

        companion object {
            fun parseMultiple(input: String): Multiple {
                require(input.startsWith("[") && input.endsWith("]"))

                val queue = input.map { it }.subList(1, input.length - 1).toMutableList().asReversed()
                val stack = mutableListOf(mutableListOf<Entry>())
                var number: Int? = null

                while (queue.isNotEmpty()) {
                    val current = queue.removeLast()

                    if (current in '0'..'9') {
                        number = (number ?: 0) * 10 + (current - '0')
                    } else {
                        if (number != null) {
                            stack.last().add(Single(number))
                            number = null
                        }
                    }

                    when (current) {
                        '[' -> stack.add(mutableListOf())
                        ']' -> stack.removeLast().let { stack.last().add(Multiple(it)) }
                    }
                }

                check(stack.size == 1)
                return Multiple(stack.first())
            }
        }
    }

    override fun parseInput(reader: BufferedReader) = reader.readText()
        .split("$sep$sep")
        .map { it.split(sep) }
        .map { (a, b) -> Entry.parseMultiple(a) to Entry.parseMultiple(b) }

    override fun solveFirstPart(): Int = input.mapIndexed { index, (a, b) -> if (a < b) (index + 1) else 0 }.sum()

    val dividerPackets = listOf(Entry.Single(2), Entry.Single(6))
    override fun solveSecondPart(): Int = input.flatMap { (a, b) -> listOf(a, b) }
        .plus(dividerPackets)
        .sorted()
        .mapIndexed { index, entry -> if (entry in dividerPackets) (index + 1) else 1 }
        .reduce { a, b -> a * b }

    companion object
}
