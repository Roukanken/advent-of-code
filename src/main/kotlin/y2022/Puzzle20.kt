package y2022

import Puzzle
import java.io.BufferedReader

class Puzzle20(path: String) : Puzzle<List<String>, Int>(path) {
    override fun parseInput(reader: BufferedReader) = generateSequence { reader.readLine() }.toList()

    override fun solveFirstPart(): Int = TODO()
    override fun solveSecondPart(): Int = TODO()

    companion object
}
