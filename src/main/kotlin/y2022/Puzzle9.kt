package y2022

import BadInput
import Location
import Puzzle
import at
import java.io.BufferedReader
import kotlin.math.sign

class Puzzle9(path: String) : Puzzle<List<Puzzle9.Command>, Int>(path) {
    class Command(val direction: Location, val steps: Int) {
        companion object {
            fun parse(input: String): Command {
                val (dir, steps) = input.split(" ")
                val direction = when (dir) {
                    "R" -> 1 at 0
                    "U" -> 0 at 1
                    "L" -> -1 at 0
                    "D" -> 0 at -1
                    else -> throw BadInput()
                }

                return Command(direction, steps.toInt())
            }
        }
    }

    override fun parseInput(reader: BufferedReader) = generateSequence { reader.readLine() }.map { Command.parse(it) }.toList()

    override fun solveFirstPart(): Int = input.asSequence()
        .genericSimulate(0 at 0, Companion::headBehavior)
        .genericSimulate(0 at 0, Companion::tailBehaviour)
        .toSet()
        .size

    override fun solveSecondPart(): Int = input.asSequence()
        .genericSimulate(0 at 0, Companion::headBehavior)
        .let { (0..<9).fold(it) { acc, _ -> acc.genericSimulate(0 at 0, Companion::tailBehaviour) } }
        .toSet()
        .size

    companion object {
        fun <Event, State> Sequence<Event>.genericSimulate(start: State, behavior: (State, Event) -> Sequence<State>): Sequence<State> =
            sequence {
                var state = start
                yield(state)

                for (event in this@genericSimulate) {
                    for (newState in behavior(state, event)) {
                        state = newState
                        yield(state)
                    }
                }
            }

        fun headBehavior(location: Location, command: Command): Sequence<Location> = location.ray(command.direction).take(command.steps)

        fun tailBehaviour(location: Location, headLocation: Location): Sequence<Location> = sequence {
            var currentLocation = location
            var vector = headLocation - currentLocation

            while (vector.x !in -1..1 || vector.y !in -1..1) {
                currentLocation += vector.x.sign at vector.y.sign
                yield(currentLocation)
                vector = headLocation - currentLocation
            }
        }
    }
}