package y2022

import Puzzle
import java.io.BufferedReader

sealed interface RockPaperScissors {
    val value: Int
    val decisionTable: List<RockPaperScissors>

    fun playAgainst(other: RockPaperScissors): Int = decisionTable.indexOf(other) * 3 + value

    object Rock : RockPaperScissors {
        override val value = 1
        override val decisionTable by lazy { listOf(Paper, Rock, Scissors) }
    }

    object Paper : RockPaperScissors {
        override val value = 2
        override val decisionTable by lazy { listOf(Scissors, Paper, Rock) }
    }

    object Scissors : RockPaperScissors {
        override val value = 3
        override val decisionTable by lazy { listOf(Rock, Scissors, Paper) }
    }

    sealed interface Strategy {
        val value: Int
        val decisionTable: List<RockPaperScissors>

        fun playAgainst(other: RockPaperScissors): Int = decisionTable.indexOf(other) + 1 + value

        object Lose : Strategy {
            override val value = 0
            override val decisionTable by lazy { listOf(Paper, Scissors, Rock) }
        }

        object Draw : Strategy {
            override val value = 3
            override val decisionTable by lazy { listOf(Rock, Paper, Scissors) }
        }

        object Win : Strategy {
            override val value = 6
            override val decisionTable by lazy { listOf(Scissors, Rock, Paper) }
        }

        companion object {
            fun parse(input: String): Strategy = when (input) {
                "X" -> Lose
                "Y" -> Draw
                "Z" -> Win
                else -> throw IllegalArgumentException("Unknown play")
            }
        }
    }

    companion object {
        fun parse(input: String): RockPaperScissors = when (input) {
            "A", "X" -> Rock
            "B", "Y" -> Paper
            "C", "Z" -> Scissors
            else -> throw IllegalArgumentException("Unknown play")
        }
    }
}

class Puzzle2(path: String) : Puzzle<List<Pair<RockPaperScissors, String>>, Int>(path) {
    override fun parseInput(reader: BufferedReader) = generateSequence { reader.readLine() }
        .map { it.split(" ") }
        .map { (left, right) -> RockPaperScissors.parse(left) to right }
        .toList()

    override fun solveFirstPart(): Int = input.sumOf { (oponent, mine) -> RockPaperScissors.parse(mine).playAgainst(oponent) }
    override fun solveSecondPart(): Int = input.sumOf { (oponent, mine) -> RockPaperScissors.Strategy.parse(mine).playAgainst(oponent) }

    companion object
}