package y2022

import Puzzle
import java.io.BufferedReader
import kotlin.math.max
import kotlin.math.min

class Puzzle4(path: String) : Puzzle<List<Pair<ClosedRange<Int>, ClosedRange<Int>>>, Int>(path) {
    override fun parseInput(reader: BufferedReader) = generateSequence { reader.readLine() }.map { it.parseRangePair() }.toList()

    override fun solveFirstPart(): Int = input.count { (left, right) -> left.fullyContains(right) || right.fullyContains(left) }
    override fun solveSecondPart(): Int = input.map { (left, right) -> left intersect right }.count { !it.isEmpty() }

    companion object {
        private fun String.parseRange(): ClosedRange<Int> {
            val (left, right) = split("-")
            return left.toInt()..right.toInt()
        }

        fun String.parseRangePair(): Pair<ClosedRange<Int>, ClosedRange<Int>> {
            val (left, right) = this.split(",")
            return left.parseRange() to right.parseRange()
        }

        fun ClosedRange<Int>.fullyContains(other: ClosedRange<Int>) = this.start <= other.start && this.endInclusive >= other.endInclusive
        infix fun ClosedRange<Int>.intersect(other: ClosedRange<Int>) = (max(this.start, other.start))..(min(this.endInclusive, other.endInclusive))
    }
}