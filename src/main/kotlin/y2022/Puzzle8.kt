package y2022

import Location
import Puzzle
import at
import get
import java.io.BufferedReader

fun <T> List<List<T>>.directionalLineStartingPoints(direction: Location): Sequence<Location> = sequence {
    val data = this@directionalLineStartingPoints

    val reverseDirection = (0 at 0).minus(direction)
    val visited = mutableSetOf<Location>()

    forEachIndexed { x, row ->
        row.forEachIndexed { y, _ ->
            val current = x at y

            val unvisitedAntiRay = current.ray(reverseDirection, includeStart = true)
                .takeWhile { it.x in data.indices && it.y in data[it.x].indices }
                .takeWhile { it !in visited }
                .toList()

            if (unvisitedAntiRay.isNotEmpty()) {
                val next = unvisitedAntiRay.last() + reverseDirection

                if (next !in visited) {
                    yield(unvisitedAntiRay.last())
                    visited += unvisitedAntiRay.toSet()
                }
            }
        }
    }
}

operator fun <T> List<List<T>>.contains(location: Location) = location.x in indices && location.y in this[location.x].indices

fun <T : Comparable<T>> List<List<T>>.visibleViaRays(rays: Iterable<Sequence<Location>>): Sequence<Location> = sequence {
    val data = this@visibleViaRays

    val visible = mutableSetOf<Location>()

    for (ray in rays) {
        var current: T? = null
        val rayVisible = ray.takeWhile { it in data }
            .filter { current == null || current!! < data[it] }
            .onEach { current = data[it] }
            .filter { it !in visible }
            .toSet()

        yieldAll(rayVisible)
        visible += rayVisible
    }
}

class Puzzle8(path: String) : Puzzle<List<List<Int>>, Int>(path) {
    override fun parseInput(reader: BufferedReader) = generateSequence { reader.readLine() }
        .map { line -> line.map { "$it".toInt() } }
        .toList()

    override fun solveFirstPart(): Int =
        input.visibleViaRays(
            Location.fourDirections.flatMap { direction ->
                input.directionalLineStartingPoints(direction).map { startingPoint -> startingPoint.ray(direction, includeStart = true) }
            }
        ).count()

    override fun solveSecondPart(): Int = input
        .flatMapIndexed { x, row -> row.indices.map { y -> x at y } }
        .maxOf { input.dumbScenicScoreAt(it) }

    companion object {
        fun List<List<Int>>.dumbScenicScoreAt(location: Location, directions: List<Location> = Location.fourDirections): Int {
            val height = this[location]

            return directions.map { direction ->
                var nextIsVisible = true

                location.ray(direction)
                    .takeWhile { it in this && nextIsVisible }
                    .onEach { nextIsVisible = this[it] < height }
                    .count()
            }.reduce { a, b -> a * b }
        }
    }
}
