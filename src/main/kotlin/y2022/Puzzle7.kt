package y2022

import BadInput
import Puzzle
import java.io.BufferedReader

sealed interface FilesystemEntry {
    val name: String

    data class Directory(override val name: String, val entries: List<FilesystemEntry>) : FilesystemEntry
    data class File(override val name: String, val size: Long) : FilesystemEntry
}

class FilesystemReconstructer(
    val root: Entry.Directory = Entry.Directory("/"),
    private var currentPath: MutableList<Entry.Directory> = mutableListOf(root)
) {
    sealed interface Entry {
        fun build(): FilesystemEntry
        val name: String

        class Directory(override val name: String, var entries: List<Entry> = emptyList()) : Entry {
            override fun build(): FilesystemEntry = FilesystemEntry.Directory(name, entries.map { it.build() })
        }

        class File(override val name: String, val size: Long) : Entry {
            override fun build(): FilesystemEntry = FilesystemEntry.File(name, size)
        }
    }

    val currentDirectory: Entry.Directory
        get() = currentPath.last()

    fun reconstruct(): FilesystemEntry = root.build()

    fun FilesystemCommand.execute() {
        when (this) {
            is FilesystemCommand.ChangeDirectory -> execute(this)
            is FilesystemCommand.ListFiles -> execute(this)
        }
    }

    fun execute(command: FilesystemCommand.ChangeDirectory) {
        when (command) {
            is FilesystemCommand.ChangeDirectory.ToRoot -> currentPath.subList(1, currentPath.size).clear()
            is FilesystemCommand.ChangeDirectory.ToOneLevelUp -> if (currentPath.size > 1) currentPath.removeLast()
            is FilesystemCommand.ChangeDirectory.ToSubdirectory -> {
                val subdirectory = currentDirectory.entries.find { it.name == command.name }
                require(subdirectory != null && subdirectory is Entry.Directory) { "Subdirectory '${command.name}' must exists" }

                currentPath.add(subdirectory)
            }
        }
    }

    fun execute(command: FilesystemCommand.ListFiles) {
        val existing = currentDirectory.entries.associateBy { it.name }

        currentDirectory.entries = command.result.map {
            when (it) {
                is FilesystemCommand.ListFiles.Entry.File -> Entry.File(it.name, it.size)
                is FilesystemCommand.ListFiles.Entry.Directory -> existing.getOrElse(it.name) { Entry.Directory(it.name) }
            }
        }
    }
}

fun reconstructFilesystem(block: FilesystemReconstructer.() -> Unit) = FilesystemReconstructer().apply { block() }.reconstruct()

sealed interface FilesystemCommand {
    data class ListFiles(val result: List<Entry>) : FilesystemCommand {
        sealed interface Entry {
            data class Directory(val name: String) : Entry
            data class File(val name: String, val size: Long) : Entry

            companion object {
                fun parse(input: String): Entry {
                    val (left, name) = input.trim().split(" ")

                    return when (left) {
                        "dir" -> Directory(name)
                        else -> File(name, left.toLong())
                    }
                }
            }
        }

        companion object {
            fun parse(input: String): ListFiles = ListFiles(
                result = input
                    .trim()
                    .split("\n")
                    .drop(1)
                    .map { Entry.parse(it) }
            )
        }
    }

    sealed interface ChangeDirectory : FilesystemCommand {
        object ToRoot : ChangeDirectory
        object ToOneLevelUp : ChangeDirectory
        data class ToSubdirectory(val name: String) : ChangeDirectory

        companion object {
            fun parse(input: String): ChangeDirectory = when (input) {
                "cd /" -> ToRoot
                "cd .." -> ToOneLevelUp
                else -> ToSubdirectory(input.substringAfter("cd "))
            }
        }
    }

    companion object {
        fun parse(input: String): FilesystemCommand = when {
            input.startsWith("cd") -> ChangeDirectory.parse(input)
            input.startsWith("ls") -> ListFiles.parse(input)
            else -> throw BadInput("unknown command '$input'")
        }

        fun parseMany(input: String): List<FilesystemCommand> = input
            .split("$")
            .drop(1)
            .map { parse(it.trim()) }
    }
}

fun FilesystemEntry.totalSizes(): Sequence<Triple<List<FilesystemEntry.Directory>, FilesystemEntry, Long>> = sequence {
    val sizes = mutableListOf(0L)
    val path = mutableListOf(FilesystemEntry.Directory("", emptyList()))
    val toProcess = mutableListOf(mutableListOf(this@totalSizes))

    while (toProcess.isNotEmpty()) {
        val current = toProcess.last().removeLast()

        when (current) {
            is FilesystemEntry.File -> {
                sizes[sizes.lastIndex] += current.size
                yield(Triple(path, current, current.size))
            }

            is FilesystemEntry.Directory -> {
                sizes.add(0)
                path.add(current)
                toProcess.add(current.entries.reversed().toMutableList())
            }
        }

        while (toProcess.isNotEmpty() && toProcess.last().isEmpty()) {
            val size = sizes.removeLast()
            yield(Triple(path, path.removeLast(), size))
            toProcess.removeLast()

            if (sizes.isNotEmpty()) {
                sizes[sizes.lastIndex] += size
            }
        }
    }
}

class Puzzle7(path: String) : Puzzle<List<FilesystemCommand>, Long>(path) {
    override fun parseInput(reader: BufferedReader) = FilesystemCommand.parseMany(reader.readText())

    override fun solveFirstPart(): Long = reconstructFilesystem { input.forEach { it.execute() } }
        .totalSizes()
        .filter { (_, entry, _) -> entry is FilesystemEntry.Directory }
        .map { (_, _, size) -> size }
        .filter { it < 100000 }
        .sum()

    override fun solveSecondPart(): Long {
        val directorySizes = reconstructFilesystem { input.forEach { it.execute() } }
            .totalSizes()
            .filter { (_, entry, _) -> entry is FilesystemEntry.Directory }
            .map { (_, _, size) -> size }
            .toList()
            .dropLast(1)

        val rootSize = directorySizes.last()

        return directorySizes.filter { 30000000 < 70000000 - rootSize + it }.min()
    }

    companion object
}
