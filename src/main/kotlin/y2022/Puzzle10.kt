package y2022

import BadInput
import ComplicatedPuzzle
import java.io.BufferedReader

interface ImmutableCommand<State> {
    fun execute(state: State): State
}

fun <State, Command : ImmutableCommand<State>> Sequence<Command>.simulate(start: State): Sequence<State> = sequence {
    var state: State = start

    yield(state)

    for (command in this@simulate) {
        state = command.execute(state)
        yield(state)
    }
}

fun Sequence<Int>.drawPixels(width: Int, fill: Char = '#', empty: Char = '.'): String {
    val image = mutableMapOf<Int, MutableSet<Int>>()

    for (pixel in this) {
        val row = pixel.div(width)
        val column = pixel.mod(width)

        image.getOrPut(row) { mutableSetOf() }.add(column)
    }

    val height = image.keys.max() + 1

    return buildString {
        for (i in 0..<height) {
            for (j in 0..<width) {
                val hasPixel = image[i]?.contains(j) ?: false
                append(if (hasPixel) fill else empty)
            }
            append('\n')
        }
    }
}

class Puzzle10(path: String) : ComplicatedPuzzle<List<Puzzle10.Command>, Int, String>(path) {
    sealed interface Command : ImmutableCommand<Pair<Int, Int>> {
        object NoOp : Command {
            override fun execute(state: Pair<Int, Int>): Pair<Int, Int> = (state.first + 1) to state.second
        }

        data class AddToX(val amount: Int) : Command {
            override fun execute(state: Pair<Int, Int>): Pair<Int, Int> = (state.first + 2) to (state.second + amount)
        }

        companion object {
            fun parse(input: String): Command =
                when (input.take(4)) {
                    "noop" -> NoOp
                    "addx" -> AddToX(input.drop(5).toInt())
                    else -> throw BadInput()
                }
        }
    }

    override fun parseInput(reader: BufferedReader) = generateSequence { reader.readLine() }.map { Command.parse(it) }.toList()

    override fun solveFirstPart(): Int = input.asSequence()
        .simulate(0 to 1)
        .sample(20, 60, 100, 140, 180, 220)
        .map { (key, value) -> key * value }
        .sum()

    override fun solveSecondPart(): String = input.asSequence()
        .simulate(0 to 1)
        .emitPixels()
        .drawPixels(40)

    companion object {
        fun <T> Sequence<Pair<Int, T>>.sample(vararg sampleTimes: Int): Map<Int, T> {
            val sampled = mutableMapOf<Int, Pair<Int, T>>()

            for ((currentTime, value) in this) {
                for (sampleTime in sampleTimes) {
                    if (currentTime >= sampleTime) continue

                    val lastSampled = sampled[sampleTime]?.first ?: -1
                    if (lastSampled > currentTime) continue

                    sampled[sampleTime] = currentTime to value
                }
            }

            return sampled.mapValues { (_, value) -> value.second }
        }

        fun Sequence<Pair<Int, Int>>.emitPixels(): Sequence<Int> = sequence {
            var lastState = 1
            var time = 0

            for ((nextTime, nextState) in this@emitPixels) {
                while (time <= nextTime) {
                    if (((time - 1).mod(40) - lastState) in -1..1)
                        yield(time)

                    time++
                }

                lastState = nextState
            }
        }
    }
}
