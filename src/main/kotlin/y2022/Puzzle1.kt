package y2022

import Puzzle
import java.io.BufferedReader

fun <T> Sequence<T>.splitBy(isDelimiter: (T) -> Boolean): Sequence<List<T>> = sequence {
    var current = mutableListOf<T>()

    forEach {
        if (isDelimiter(it)) {
            yield(current)
            current = mutableListOf()
        } else {
            current += it
        }
    }

    if (current.isNotEmpty()) {
        yield(current)
    }
}

class Puzzle1(path: String) : Puzzle<List<List<Int>>, Int>(path) {
    override fun parseInput(reader: BufferedReader) = generateSequence { reader.readLine() }
        .splitBy { it == "" }
        .map { it.map(String::toInt) }
        .toList()

    override fun solveFirstPart(): Int = input.maxOf { it.sum() }
    override fun solveSecondPart(): Int = input.map { it.sum() }.sortedDescending().take(3).sum()

    companion object
}