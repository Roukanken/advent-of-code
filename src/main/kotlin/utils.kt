val sep: String = System.lineSeparator()

class Reader(path: String) {
    private val content = this::class.java.getResource(path).readText()
    private val reader = content.reader().buffered()

    fun readLineAsInt(): Int? {
        val readLine: String? = reader.readLine()
        return readLine?.toInt()
    }

    fun readLine(): String? {
        return reader.readLine()
    }
}

class SolutionNotFound : Exception("Solution not found")
class BadInput(reason: String? = null) : Exception("Bad input" + (reason?.let { ": $it" } ?: ""))
class UnreachableCode : Exception("Unreachable code")
class NoIdea : Exception("No idea how to solve this")

fun <T> Map<T, List<T>>.computeReachable(start: T): Set<T> = computeReachable(start) { this[it] ?: emptyList() }

fun <T> computeReachable(start: T, getNeighborhood: (T) -> Iterable<T>): Set<T> {
    val reachable = mutableSetOf<T>()
    val work = mutableListOf(start)

    while (work.isNotEmpty()) {
        val current = work.removeLast()
        if (current in reachable) continue

        reachable.add(current)
        getNeighborhood(current).forEach {
            work.add(it)
        }
    }

    return reachable
}

infix fun <A, B> Iterable<A>.product(other: Iterable<B>): Sequence<Pair<A, B>> = sequence {
    for (a in this@product) {
        for (b in other) {
            yield(a to b)
        }
    }
}

interface Coordinates<T> {
    operator fun plus(other: T): T
    fun neighborhood(): Sequence<T>
}

data class Location(
    val x: Int,
    val y: Int
) : Coordinates<Location> {
    override operator fun plus(other: Location): Location {
        val x = this.x + other.x
        val y = this.y + other.y
        return Location(x, y)
    }

    operator fun minus(other: Location): Location = this + (other * -1)

    fun ray(direction: Location, includeStart: Boolean = false) = generateSequence(this) { it + direction }.drop(if (includeStart) 0 else 1)

    override fun neighborhood() = eightDirections.asSequence()
        .map { it + this }

    fun fourDirectionNeighborhood() = fourDirections.map { it + this }

    operator fun times(amount: Int): Location {
        return x * amount at y * amount
    }

    companion object {
        val eightDirections = listOf(
            -1 at -1,
            0 at -1,
            1 at -1,
            -1 at 0,
            1 at 0,
            -1 at 1,
            0 at 1,
            1 at 1
        )

        val fourDirections = listOf(
            1 at 0,
            0 at -1,
            -1 at 0,
            0 at 1
        )
    }

}

operator fun <T> List<List<T>>.get(location: Location): T = this[location.x][location.y]
fun <T> List<List<T>>.locations() = indices.flatMap { x -> this[0].indices.map { y -> y at x } }
fun <T> List<List<T>>.getOrNull(location: Location): T? = this.getOrNull(location.x)?.getOrNull(location.y)
fun <T> List<List<T>>.getOrElse(location: Location, default: (Location) -> T): T = this.getOrNull(location) ?: default(location)
infix fun <T> List<List<T>>.hasIndex(location: Location): Boolean = location.x in indices && location.y in this[location.x].indices

operator fun <T> MutableList<MutableList<T>>.set(location: Location, value: T) {
    this[location.y][location.x] = value
}

infix fun Int.at(i: Int): Location {
    return Location(this, i)
}

fun <T> List<List<T>>.transpose(): List<List<T>> {
    if (isEmpty()) return this
    require(all { it.size == this[0].size }) { "Size of all lists must be equal to transpose" }

    return this[0].indices.map { col ->
        indices.map { row -> this[row][col] }
    }
}
fun <T> List<List<T>>.transpose(fill: T): List<List<T>> {
    val maxSize = maxOf { it.size }
    return map { it + List(maxSize - it.size) { fill } }.transpose()
}
fun gcd(a: Int, b: Int): Int = if (b == 0) a else gcd(b, a % b)
