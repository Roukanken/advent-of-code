package y2020

import Puzzle
import java.io.BufferedReader

sealed class BitmaskStatement {

    abstract fun execute(state: BitComputer)
    abstract fun executeV2(state: BitComputer)

    data class AssignMask(val value: Mask) : BitmaskStatement() {

        override fun execute(state: BitComputer) {
            state.mask = value
        }

        override fun executeV2(state: BitComputer) {
            state.mask = value
        }

        companion object {
            fun parse(text: String): AssignMask {
                val matchResult = Regex("mask = ([01X]+)").matchEntire(text)
                require(matchResult != null) { "mask assigment does not match regex: \"$text\"" }

                val (_, mask) = matchResult.groupValues

                val value = mask.map {
                    when (it) {
                        'X' -> null
                        '1' -> true
                        '0' -> false
                        else -> throw IllegalArgumentException()
                    }
                }.reversed()

                return AssignMask(Mask(value))
            }
        }
    }

    data class AssignMemory(val address: Long, val value: Long) : BitmaskStatement() {
        override fun execute(state: BitComputer) {
            state.memory[address] = state.mask.apply(value)
        }

        override fun executeV2(state: BitComputer) {
            val addresses = floating(state.mask.applyV2(address)).toList()
            for (realAddress in addresses) {
                state.memory[realAddress] = value
            }
        }

        companion object {
            fun parse(text: String): AssignMemory {
                val matchResult = Regex("mem\\[(\\d+)\\] = (\\d+)").matchEntire(text)
                require(matchResult != null) { "memory assigment does not match regex: \"$text\"" }

                val (_, address, value) = matchResult.groupValues
                return AssignMemory(address.toLong(), value.toLong())
            }
        }
    }

    companion object {
        fun parse(text: String) = when {
            text.startsWith("mem[") -> AssignMemory.parse(text)
            text.startsWith("mask = ") -> AssignMask.parse(text)
            else -> throw IllegalArgumentException()
        }
    }
}

data class BitComputer(var mask: Mask = Mask(emptyList()), val memory: MutableMap<Long, Long> = mutableMapOf())

data class Mask(val value: List<Boolean?>) {
    fun apply(value: Long): Long {
        return this.value.foldIndexed(value) { index, current, maskBit ->
            when (maskBit) {
                true -> current.or(1L.shl(index))
                false -> current.and(1L.shl(index).inv())
                null -> current
            }
        }
    }

    fun applyV2(value: Long): List<Boolean?> {
        return this.value.mapIndexed { index, maskBit ->
            when (maskBit) {
                true -> true
                false -> value.and(1L.shl(index)) != 0L
                null -> null
            }
        }
    }
}

fun floating(value: List<Boolean?>, index: Int = 0, acc: Long = 0): List<Long> {
    if (index !in value.indices) return listOf(acc)

    return when (value[index]) {
        true -> floating(value, index + 1, acc + 1L.shl(index))
        false -> floating(value, index + 1, acc)
        null -> floating(value, index + 1, acc) + floating(value, index + 1, acc + 1L.shl(index))
    }
}

class Puzzle14(path: String) : Puzzle<List<BitmaskStatement>, Long>(path) {
    override fun parseInput(reader: BufferedReader) =
        reader.readLines().map { BitmaskStatement.parse(it) }

    override fun solveFirstPart(): Long {
        val computer = BitComputer()
        input.forEach { it.execute(computer) }
        return computer.memory.values.sum()
    }

    override fun solveSecondPart(): Long {
        val computer = BitComputer()
        input.forEach { it.executeV2(computer) }
        return computer.memory.values.sum()
    }
}