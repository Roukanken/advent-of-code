package y2020

import Puzzle
import java.io.BufferedReader

class Puzzle15(path: String) : Puzzle<List<Long>, Long>(path) {
    override fun parseInput(reader: BufferedReader) =
        reader.readLine()
            .trim()
            .split(",")
            .map { it.toLong() }

    override fun solveFirstPart(): Long {
        return memoryGame(input).drop(2019).first()
    }

    override fun solveSecondPart(): Long {
        return memoryGame(input).drop(30000000 - 1).first()
    }

    companion object {
        fun memoryGame(input: List<Long>) = sequence {
            yieldAll(input)

            val state = input.withIndex()
                .takeWhile { (i, x) -> i != input.size - 1 }
                .associateTo(mutableMapOf()) { (i, x) -> x to i.toLong() }

            var index = input.size.toLong() - 1
            var last = input.last()
            while (true) {
                val lastIndex = state[last]
                state[last] = index
                last = if (lastIndex == null) 0 else index - lastIndex
                index++

                yield(last)
            }
        }
    }
}