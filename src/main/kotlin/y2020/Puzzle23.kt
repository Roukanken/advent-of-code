package y2020

import Puzzle
import java.io.BufferedReader

class Cups(cups: List<Int>) {

    val data = cups.let {
        val data = MutableList(it.size + 1) { Cup(0, 0) }

        for (index in it.indices) {
            val nextIndex = if (index + 1 in it.indices) index + 1 else 0
            data[it[index]] = Cup(it[index], it[nextIndex])
        }

        data
    }

    private var current = data[cups[0]]

    private fun takeCups(n: Int, now: Cup = current) = (0 until n - 1).scan(now) { it, _ -> data[it.next] }

    fun simulateRound() {
        val moving = takeCups(4).drop(1)
        val movingValues = moving.map { it.value }

        var targetValue = if (current.value == 1) data.lastIndex else current.value - 1
        while (targetValue in movingValues)
            targetValue = if (targetValue == 1) data.lastIndex else targetValue - 1

        val target = data[targetValue]

        current.next = moving.last().next
        moving.last().next = target.next
        target.next = moving[0].value

        current = data[current.next]
    }

    fun toList(): List<Int> = takeCups(data.size - 1).map { it.value }

    data class Cup(val value: Int, var next: Int)
}

class Puzzle23(path: String) : Puzzle<List<Int>, Long>(path) {
    override fun parseInput(reader: BufferedReader) =
        reader.readLine()
            .trim()
            .map { it.toInt() - '0'.toInt() }

    override fun solveFirstPart(): Long {
        val cups = Cups(input)
        (0 until 100).forEach { _ -> cups.simulateRound() }

        var result = cups.toList()
        val turnedAround = result.indexOf(1)
        result = result.drop(turnedAround + 1) + result.take(turnedAround)

        return result.joinToString(separator = "").toLong()
    }

    override fun solveSecondPart(): Long {
        val cups = Cups(input + ((input.size + 1)..1000000))
        (0 until 10000000).forEach { _ -> cups.simulateRound() }

        val result = cups.toList()
        val turnedAround = result.indexOf(1)
        val (one, two) = result.drop(turnedAround + 1).take(2)

        return one.toLong() * two.toLong()
    }
}

