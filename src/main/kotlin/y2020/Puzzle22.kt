package y2020

import Puzzle
import sep
import java.io.BufferedReader
import java.lang.Integer.max
import java.lang.Integer.min

class Puzzle22(path: String) : Puzzle<Pair<List<Int>, List<Int>>, Int>(path) {
    override fun parseInput(reader: BufferedReader): Pair<List<Int>, List<Int>> {
        val (p1, p2) = reader.readText()
            .trim()
            .split("$sep$sep")
            .map { parseDeck(it) }

        return p1 to p2
    }

    private fun parseDeck(text: String): List<Int> {
        return text.split(sep)
            .drop(1)
            .map { it.toInt() }
    }

    override fun solveFirstPart(): Int {
        val deck1 = input.first.toCollection(ArrayDeque())
        val deck2 = input.second.toCollection(ArrayDeque())

        while (deck1.isNotEmpty() && deck2.isNotEmpty()) {
            val card1 = deck1.removeFirst()
            val card2 = deck2.removeFirst()

            val target = if (card1 > card2) deck1 else deck2
            target.addLast(max(card1, card2))
            target.addLast(min(card1, card2))
        }

        val winningDeck = if (deck1.isEmpty()) deck2 else deck1

        return winningDeck.reversed()
            .mapIndexed { index, card -> (index + 1) * card }
            .sum()
    }

    fun recursiveCombat(deck1: ArrayDeque<Int>, deck2: ArrayDeque<Int>): Pair<Int, ArrayDeque<Int>> {
        val visited: MutableSet<Pair<ArrayDeque<Int>, ArrayDeque<Int>>> = mutableSetOf()

        while (deck1.isNotEmpty() && deck2.isNotEmpty()) {
            if (deck1 to deck2 in visited) {
                return 1 to deck2
            }
            visited.add(deck1 to deck2)

            val card1 = deck1.removeFirst()
            val card2 = deck2.removeFirst()

            val winner = if (deck1.size >= card1 && deck2.size >= card2) {
                val newDeck1 = deck1.take(card1).toCollection(ArrayDeque())
                val newDeck2 = deck2.take(card2).toCollection(ArrayDeque())

                val (recursiveWinner, _) = recursiveCombat(newDeck1, newDeck2)
                recursiveWinner
            } else {
                if (card1 > card2) 1 else 2
            }

            if (winner == 1) {
                deck1.addLast(card1)
                deck1.addLast(card2)
            } else {
                deck2.addLast(card2)
                deck2.addLast(card1)
            }
        }

        return if (deck1.isNotEmpty()) 1 to deck1 else 2 to deck2
    }

    override fun solveSecondPart(): Int {
        val deck1 = input.first.toCollection(ArrayDeque())
        val deck2 = input.second.toCollection(ArrayDeque())

        val (_, winningDeck) = recursiveCombat(deck1, deck2)

        return winningDeck.reversed()
            .mapIndexed { index, card -> (index + 1) * card }
            .sum()
    }
}

