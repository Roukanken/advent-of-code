package y2020

import Coordinates
import Puzzle
import java.io.BufferedReader

data class Location3d(
    val x: Int,
    val y: Int,
    val z: Int,
) : Coordinates<Location3d> {

    override operator fun plus(other: Location3d): Location3d {
        val x = this.x + other.x
        val y = this.y + other.y
        val z = this.z + other.z
        return Location3d(x, y, z)
    }

    override fun neighborhood() = allDirections.asSequence()
        .map { it + this }

    companion object {
        val allDirections = sequence {
            for (x in -1..1)
                for (y in -1..1)
                    for (z in -1..1)
                        yield(Location3d(x, y, z))
        }
            .filter { it != Location3d(0, 0, 0) }
            .toList()
    }
}

data class Location4d(
    val x: Int,
    val y: Int,
    val z: Int,
    val w: Int,
) : Coordinates<Location4d> {

    override operator fun plus(other: Location4d): Location4d {
        val x = this.x + other.x
        val y = this.y + other.y
        val z = this.z + other.z
        val w = this.w + other.w
        return Location4d(x, y, z, w)
    }

    override fun neighborhood() = allDirections.asSequence()
        .map { it + this }

    companion object {
        val allDirections = sequence {
            for (x in -1..1)
                for (y in -1..1)
                    for (z in -1..1)
                        for (w in -1..1)
                            yield(Location4d(x, y, z, w))
        }
            .filter { it != Location4d(0, 0, 0, 0) }
            .toList()
    }
}

fun Location3d.to4d() = Location4d(x, y, z, 0)


class ConwaysGameOfLife<T : Coordinates<T>>(field: Set<T> = setOf()) :
    GenericCellAutomata<T>(field, GenericNeighborhoodsRule(3..3, 2..3))

class Puzzle17(path: String) : Puzzle<Set<Location3d>, Int>(path) {
    override fun parseInput(reader: BufferedReader) =
        reader.readLines().flatMapIndexed { y, row ->
            row.mapIndexedNotNull { x, char -> if (char == '#') Location3d(x, y, 0) else null }
        }.toSet()

    override fun solveFirstPart(): Int {
        val final = generateSequence(ConwaysGameOfLife(input) as GenericCellAutomata<Location3d>) { it.next() }
            .take(7)
            .last()

        return final.field.size
    }

    override fun solveSecondPart(): Int {
        val set = input.map { it.to4d() }.toSet()
        val final = generateSequence(ConwaysGameOfLife(set) as GenericCellAutomata<Location4d>) { it.next() }
            .take(7)
            .last()

        return final.field.size
    }
}

