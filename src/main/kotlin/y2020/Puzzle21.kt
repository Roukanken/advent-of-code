package y2020

import Puzzle
import java.io.BufferedReader

data class Recipe(val ingredients: List<String>, var allergens: List<String>) {

    companion object {
        fun parse(text: String): Recipe {
            val (ingredients, allergens) = text.trimEnd(')').split(" (contains ")

            return Recipe(
                ingredients = ingredients.split(" "),
                allergens = allergens.split(", ")
            )
        }
    }
}

class Puzzle21(path: String) : Puzzle<List<Recipe>, String>(path) {
    override fun parseInput(reader: BufferedReader) =
        reader.readLines()
            .map { Recipe.parse(it) }

    private val allIngredients = input.fold(setOf<String>()) { acc, recipe -> acc.union(recipe.ingredients) }

    private val allergenStats = input.flatMap { recipe ->
        recipe.allergens.map { it to recipe.ingredients.toMutableSet() }
    }
        .groupingBy { it.first }
        .fold(allIngredients) { accumulator, element -> accumulator.intersect(element.second) }

    private val allergenIngredients = allergenStats.values.reduce { a, b -> a.union(b) }

    override fun solveFirstPart(): String {
        return input.flatMap { it.ingredients }
            .filter { it !in allergenIngredients }
            .count()
            .toString()
    }

    override fun solveSecondPart(): String {
        var allergens = allergenStats.toMutableMap()

        val result = mutableMapOf<String, String>()
        do {
            val determinedValues = allergens.filterValues { it.size == 1 }
                .filterValues { it.first() !in result }
                .mapValues { (_, value) -> value.first() }

            determinedValues.entries.forEach { (allergen, ingredient) ->
                result[allergen] = ingredient
                allergens =
                    allergens.mapValues { (_, value) -> value.filter { it != ingredient }.toSet() }.toMutableMap()
            }

        } while (result.size < allergens.size)

        return result.entries
            .sortedBy { it.key }
            .joinToString(",") { it.value }
    }
}

