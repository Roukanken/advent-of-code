package y2020

import Location
import Puzzle
import at
import java.io.BufferedReader

enum class Object {
    TREE, EMPTY;

    companion object {
        fun parse(c: Char): Object {
            return when (c) {
                '.' -> EMPTY
                '#' -> TREE
                else -> throw IllegalArgumentException("Not an object")
            }
        }
    }
}

class Forest(private val map: List<List<Object>>) {
    operator fun get(location: Location): Object {
        return map[location.y][location.x % size.x]
    }

    private val size = Location(map[0].size, map.size)

    init {
        require(map.all { it.size == size.x }) { "Not all rows in map have same size!" }
    }

    fun toboggan(slope: Location): Sequence<Object> {
        return generateSequence(0 at 0) { it + slope }
            .takeWhile { it.y < size.y }
            .map { get(it) }
    }

    companion object {
        fun parse(input: String): Forest {
            val map = input.split("\n").asSequence()
                .map { it.trim() }
                .map {
                    it.asSequence()
                        .map { c -> Object.parse(c) }
                        .toList()
                }
                .toList()

            return Forest(map)
        }
    }
}

class Puzzle3(path: String) : Puzzle<Forest, Int>(path) {
    override fun parseInput(reader: BufferedReader): Forest {
        return Forest.parse(reader.readText().trim())
    }

    private fun countTobogganTrees(slope: Location) = input.toboggan(slope)
        .count { it == Object.TREE }

    override fun solveFirstPart(): Int {
        return countTobogganTrees(3 at 1)
    }

    override fun solveSecondPart(): Int {
        return countTobogganTrees(1 at 1) * countTobogganTrees(3 at 1) *
                countTobogganTrees(5 at 1) * countTobogganTrees(7 at 1) *
                countTobogganTrees(1 at 2)
    }

}