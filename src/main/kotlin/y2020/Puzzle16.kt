package y2020

import Puzzle
import sep
import java.io.BufferedReader

data class TicketInput(
    val attributes: List<TicketAttribute>,
    val myTicket: Ticket,
    val otherTickets: List<Ticket>,
) {

    companion object {
        fun parse(text: String): TicketInput {
            val (attributes, myTicket, otherTickets) = text.split("$sep$sep")

            return TicketInput(
                attributes = attributes.split(sep).map { TicketAttribute.parse(it) },
                myTicket = Ticket.parse(myTicket.split(sep)[1]),
                otherTickets = otherTickets.split(sep).drop(1).map { Ticket.parse(it) },
            )
        }
    }
}

data class Ticket(val attributeValues: List<Int>) {
    companion object {
        fun parse(text: String): Ticket {
            val values = text.split(",")
                .map { it.toInt() }

            return Ticket(values)
        }
    }
}

data class TicketAttribute(
    val name: String,
    val isValueValid: (Int) -> Boolean
) {
    companion object {
        fun parse(text: String): TicketAttribute {
            val (name, ranges) = text.split(": ")
            val (range1, range2) = ranges.split(" or ")

            return TicketAttribute(
                name = name,
                isValueValid = parseRange(range1) or parseRange(range2)
            )
        }

        private fun parseRange(text: String): (Int) -> Boolean {
            val (low, high) = text.split("-")
            val range = low.toInt()..high.toInt()
            return { it in range }
        }
    }
}

class Puzzle16(path: String) : Puzzle<TicketInput, Long>(path) {
    override fun parseInput(reader: BufferedReader) =
        TicketInput.parse(reader.readText().trim())

    private val isValueValid = input.attributes.map { it.isValueValid }
        .reduce { a, b -> a or b }

    override fun solveFirstPart(): Long {
        return input.otherTickets
            .mapNotNull { it.attributeValues.firstOrNull(!isValueValid) }
            .sum()
            .toLong()
    }

    override fun solveSecondPart(): Long {
        val validTickets = input.otherTickets.filter { it.attributeValues.all(isValueValid) }

        var allValid = mutableMapOf<Int, Set<TicketAttribute>>()
        for (position in input.myTicket.attributeValues.indices) {
            val valid = validTickets.fold(input.attributes) { acc, ticket ->
                acc.filter { it.isValueValid(ticket.attributeValues[position]) }
            }

            allValid[position] = valid.toSet()
        }

        val result = mutableMapOf<TicketAttribute, Int>()
        do {
            val determinedValues = allValid.filterValues { it.size == 1 }
                .filterValues { it.first() !in result }
                .mapValues { (_, value) -> value.first() }

            determinedValues.entries.forEach { (pos, attr) ->
                result[attr] = pos
                allValid = allValid.mapValues { (_, value) -> value.filter { it != attr }.toSet() }.toMutableMap()
            }

        } while (result.size < allValid.size)

        return result.filter { (key, _) -> key.name.startsWith("departure") }
            .values
            .map { value -> input.myTicket.attributeValues[value].toLong() }
            .reduce { a, b -> a * b }
    }
}

inline infix fun <T> ((T) -> Boolean).or(crossinline other: (T) -> Boolean) = { arg: T -> this(arg) || other(arg) }
inline operator fun <T> ((T) -> Boolean).not() = { arg: T -> !this(arg) }