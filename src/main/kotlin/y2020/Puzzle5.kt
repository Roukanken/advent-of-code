package y2020

import Puzzle
import SolutionNotFound
import java.io.BufferedReader
import java.lang.IllegalArgumentException

class Puzzle5(path: String) : Puzzle<List<Int>, Int>(path) {
    override fun parseInput(reader: BufferedReader): List<Int> {
        return reader.readLines()
            .map { str -> str.map { transformToBinary(it) } }
            .map { it.joinToString("") }
            .map { it.toInt(2) }
    }

    private fun transformToBinary(it: Char) = when (it) {
        'F', 'L' -> '0'
        'B', 'R' -> '1'
        else -> throw IllegalArgumentException()
    }

    override fun solveFirstPart(): Int {
        return input.maxOrNull() ?: throw SolutionNotFound()
    }

    override fun solveSecondPart(): Int {
        val seats = input.toSet()

        for(seat in 0..1023) {
            if (seat in seats) {
                continue
            }

            if (seat - 1 in seats && seat +1 in seats) {
                return seat
            }
        }

        throw SolutionNotFound()
    }
}
