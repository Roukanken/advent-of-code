package y2020

import Puzzle
import computeReachable
import java.io.BufferedReader

class Puzzle7(path: String) : Puzzle<Map<Puzzle7.Color, Puzzle7.Bag>, Int>(path) {

    class Bag(val color: Color, val children: Map<Color, Int>) {

        override fun toString(): String {
            return "Bag(color=${color.value})"
        }

        companion object {
            fun parse(text: String): Bag {
                val (colorText, childrenText) = text.split(" contain ")
                val color = Color.parse(colorText)

                val children = if (childrenText != "no other bags.") {
                    childrenText.split(", ")
                        .map { it.split(" ", limit = 2) }
                        .associate { (count, color) -> Color.parse(color) to count.toInt() }
                } else {
                    mapOf()
                }

                return Bag(color, children)
            }
        }
    }

    data class Color(val value: String) {
        companion object {
            fun parse(value: String): Color {
                val color = value.replace(" bags?.?$".toRegex(), "")
                return Color(color)
            }
        }
    }

    override fun parseInput(reader: BufferedReader): Map<Color, Bag> {
        return reader.readLines()
            .filter { it.isNotBlank() }
            .map { Bag.parse(it) }
            .associateBy { it.color }
    }

    override fun solveFirstPart(): Int {
        return constructReverseMap(input)
            .computeReachable(Color.parse("shiny gold bag"))
            .size - 1
    }

    private val bagCounts = mutableMapOf<Color, Int>()

    fun countContents(color: Color): Int {
        val bag = input[color] ?: throw IllegalArgumentException()

        return bag.children.map { (color, count) ->
            count * (bagCounts.getOrPut(color, { countContents(color) }) + 1)
        }.sum()
    }

    override fun solveSecondPart(): Int {
        return countContents(Color.parse("shiny gold bag"))
    }

    companion object {
        fun constructReverseMap(map: Map<Color, Bag>) = map.values
            .flatMap { it.children.keys.map { child -> child to it.color } }
            .groupBy({ (child, _) -> child }, { (_, parent) -> parent })
    }
}