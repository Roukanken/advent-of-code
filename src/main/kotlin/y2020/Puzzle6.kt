package y2020

import Puzzle
import sep
import java.io.BufferedReader
import java.util.*

class Puzzle6(path: String) : Puzzle<List<Puzzle6.Group>, Int>(path) {

    data class Group(val people: List<Person>) {

        fun anyQuestion(): Set<Question> {
            return people.map { it.answers }
                .reduce { left, right -> left.union(right)}
        }

        fun allQuestion(): Set<Question> {
            return people.map { it.answers }
                .reduce { left, right -> left.intersect(right)}
        }

        companion object {
            fun parse(text: String): Group {
                val people = text.trim()
                    .split(sep)
                    .map { Person.parse(it) }

                return Group(people)
            }
        }
    }

    class Person(val answers: Set<Question>) {
        companion object {
            fun parse(text: String): Person {
                val answers = text.trim()
                    .toCharArray()
                    .asSequence()
                    .map { Question.parse(it) }
                    .toEnumSet()

                return Person(answers)
            }
        }
    }

    enum class Question(val value: Char) {
        A('a'),
        B('b'),
        C('c'),
        D('d'),
        E('e'),
        F('f'),
        G('g'),
        H('h'),
        I('i'),
        J('j'),
        K('k'),
        L('l'),
        M('m'),
        N('n'),
        O('o'),
        P('p'),
        Q('q'),
        R('r'),
        S('s'),
        T('t'),
        U('u'),
        V('v'),
        W('w'),
        X('x'),
        Y('y'),
        Z('z'),
        ;

        companion object {
            fun parse(value: Char): Question {
                return values()
                    .first { it.value == value }
            }
        }
    }

    override fun parseInput(reader: BufferedReader): List<Group> {
        return reader.readText()
            .split("$sep$sep")
            .map { Group.parse(it) }
    }

    override fun solveFirstPart(): Int {
        return input
            .map { it.anyQuestion().size }
            .sum()
    }

    override fun solveSecondPart(): Int {
        return input
            .map { it.allQuestion().size }
            .sum()
    }
}

inline fun <reified T: Enum<T>> Sequence<T>.toEnumSet(): EnumSet<T> {
    return toCollection(EnumSet.noneOf(T::class.java))
}
