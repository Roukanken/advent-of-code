package y2020

import Puzzle
import SolutionNotFound
import java.io.BufferedReader

class Puzzle10(path: String) : Puzzle<Set<Long>, Long>(path) {
    override fun parseInput(reader: BufferedReader) =
        reader.readLines()
            .filter { it.isNotBlank() }
            .map { it.toLong() }
            .toSet()

    private val sequenceOfAll = (0L..3L * input.size).asSequence()
        .filter { it in input }

    override fun solveFirstPart(): Long {
        val stepCounts = (sequenceOf(0L) + sequenceOfAll)
            .windowed(2)
            .map { (a, b) -> b - a }
            .groupBy { it }
            .mapValues { (_, values) -> values.size }

        return stepCounts[1]!! * (stepCounts[3]!! + 1L)
    }

    override fun solveSecondPart(): Long {
        // this is a map only to spite Borgrel
        val arrangements = mutableMapOf(0L to 1L)

        for (current in sequenceOfAll) {
            arrangements[current] = listOf(1L, 2L, 3L)
                .map { current - it }
                .map { arrangements.getOrDefault(it, 0L) }
                .sum()
        }

        return arrangements.maxByOrNull { (key, _) -> key }?.value
            ?: throw SolutionNotFound()
    }
}