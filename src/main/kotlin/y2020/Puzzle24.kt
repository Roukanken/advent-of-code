package y2020

import Coordinates
import Puzzle
import java.io.BufferedReader

abstract class GenericAutomataRule<T : Coordinates<T>> {
    abstract operator fun invoke(automata: GenericCellAutomata<T>, location: T): Boolean
}

class GenericNeighborhoodsRule<T : Coordinates<T>>(
    private val deadToAlive: ClosedRange<Int>,
    private val aliveToAlive: ClosedRange<Int>
) : GenericAutomataRule<T>() {

    override fun invoke(automata: GenericCellAutomata<T>, location: T): Boolean {
        val aliveNeighborhoods = location.neighborhood().count { it in automata }
        return if (location in automata) aliveNeighborhoods in aliveToAlive else aliveNeighborhoods in deadToAlive
    }
}

open class GenericCellAutomata<T : Coordinates<T>>(open val field: Set<T> = setOf(), open val rule: GenericAutomataRule<T>) {

    open fun next(): GenericCellAutomata<T> {
        val activeCells = field.flatMap { it.neighborhood() }.toSet()
        val alive = activeCells.filter { cell -> rule(this, cell) }.toSet()
        return GenericCellAutomata(alive, rule)
    }

    open operator fun contains(cell: T): Boolean = cell in field
}


data class HexagonLocation(val x: Int, val y: Int, val z: Int) : Coordinates<HexagonLocation> {
    override operator fun plus(other: HexagonLocation) = HexagonLocation(
        this.x + other.x,
        this.y + other.y,
        this.z + other.z,
    )

    override fun neighborhood(): Sequence<HexagonLocation> =
        HexagonDirection.values()
            .map { it.vector }
            .map { it + this }
            .asSequence()

}

data class HexagonPath(val path: List<HexagonDirection>) {

    fun destination() = path.map { it.vector }.reduce { a, b -> a + b }

    companion object {
        fun parse(text: String): HexagonPath {
            val path = text.replace("e", "e ")
                .replace("w", "w ")
                .trim()
                .split(" ")
                .map { HexagonDirection.fromCode(it) }

            return HexagonPath(path)
        }
    }
}

enum class HexagonDirection(val code: String, val vector: HexagonLocation) {
    WEST("w", HexagonLocation(-1, -1, 0)),
    SOUTH_WEST("sw", HexagonLocation(0, -1, -1)),
    NORTH_WEST("nw", HexagonLocation(-1, 0, 1)),
    EAST("e", HexagonLocation(1, 1, 0)),
    SOUTH_EAST("se", HexagonLocation(1, 0, -1)),
    NORTH_EAST("ne", HexagonLocation(0, 1, 1)),
    ;

    companion object {
        fun fromCode(code: String) = values().first { it.code == code }
    }
}

class Puzzle24(path: String) : Puzzle<List<HexagonPath>, Long>(path) {
    override fun parseInput(reader: BufferedReader) =
        reader.readLines()
            .map { it.trim() }
            .filter { it.isNotEmpty() }
            .map { HexagonPath.parse(it) }

    override fun solveFirstPart(): Long {
        return input.map { it.destination() }
            .groupingBy { it }
            .eachCount()
            .filterValues { it % 2 == 1 }
            .count()
            .toLong()
    }

    override fun solveSecondPart(): Long {
        val startingAlive = input.map { it.destination() }
            .groupingBy { it }
            .eachCount()
            .filterValues { it % 2 == 1 }
            .keys

        val automata = GenericCellAutomata(startingAlive, GenericNeighborhoodsRule(2..2, 1..2))

        return generateSequence(automata) { it.next() }
            .drop(100)
            .first()
            .field.size.toLong()
    }
}

