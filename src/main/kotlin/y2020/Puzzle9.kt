package y2020

import Puzzle
import SolutionNotFound
import y2020.Instruction.*
import java.io.BufferedReader

class Puzzle9(path: String) : Puzzle<List<Long>, Long>(path) {
    override fun parseInput(reader: BufferedReader) =
        reader.readLines()
            .filter { it.isNotBlank() }
            .map { it.toLong() }

    override fun solveFirstPart(): Long {
        return input.asSequence()
            .windowed(25, transform = { it.toSet() })
            .zip(input.asSequence().drop(25))
            .filterNot { (window, now) -> window.any { window.contains(now - it) } }
            .map { (_, now) -> now }
            .first()
    }

    override fun solveSecondPart(): Long {
        val targetSum = solveFirstPart()
        val current = LongSetWithSum()

        val (start, end) = input.iterator() to input.iterator()

        while (true) {
            when {
                current.sum < targetSum -> current.add(end.next())
                current.sum == targetSum -> return current.minOrNull()!! + current.maxOrNull()!!
                current.sum > targetSum -> current.remove(start.next())
            }
        }
    }

    class LongSetWithSum(
        val set: MutableSet<Long> = mutableSetOf(),
        var sum: Long = set.sum()
    ) : MutableSet<Long> by set {

        override fun add(element: Long) = set.add(element).also { sum += element }
        override fun remove(element: Long) = set.remove(element).also { sum -= element }
    }
}