package y2020

import Puzzle
import java.io.BufferedReader

class Puzzle2(path: String) : Puzzle<Iterable<Entry>, Int>(path) {

    override fun parseInput(reader: BufferedReader) =
        generateSequence { reader.readLine() }
        .map { Entry.parse(it) }
        .toList()

    override fun solveFirstPart(): Int {
        return input.filter { it.isValidOld() }.size
    }

    override fun solveSecondPart(): Int {
        return input.filter { it.isValidNew() }.size

    }

}

@JvmInline
value class Password(private val password: String) {
    fun countCharacters() = password.toCharArray()
        .toList()
        .groupingBy { it }
        .eachCount()

    operator fun get(index: Int): Char = password[index - 1]
}

data class Policy(
    val range: ClosedRange<Int>,
    val character: Char
) {

    fun isValidOld(password: Password): Boolean {
        val characterCounts = password.countCharacters()
        return characterCounts.getOrDefault(character, 0) in range
    }

    fun isValidNew(password: Password): Boolean {
        val chars = setOf(password[range.start], password[range.endInclusive])
        return character in chars && chars.size == 2
    }

    companion object {
        fun parse(text: String): Policy {
            val (rangeText, character) = text.split(" ")
            require(character.length == 1)

            val (low, high) = rangeText.split("-")
            val range = low.toInt()..high.toInt()

            return Policy(range, character[0])
        }
    }
}


data class Entry(
    val policy: Policy,
    val password: Password,
) {

    fun isValidOld() = policy.isValidOld(password)
    fun isValidNew() = policy.isValidNew(password)

    companion object {
        fun parse(text: String): Entry {
            val (policy, password) = text.split(": ")

            return Entry(Policy.parse(policy), Password(password))
        }

    }
}


