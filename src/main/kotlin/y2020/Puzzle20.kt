package y2020

import Puzzle
import sep
import java.io.BufferedReader

data class JigsawEdge(val row: List<Boolean>) : Comparable<JigsawEdge> {

    companion object {
        fun from(row: List<Boolean>): JigsawEdge {
            return minOf(JigsawEdge(row.reversed()), JigsawEdge(row))
        }

        fun fromTile(tile: JigsawTile): List<JigsawEdge> {
            return listOf(
                from(tile.grid.first()),
                from(tile.grid.map { it.last() }),
                from(tile.grid.last()),
                from(tile.grid.map { it.first() }),
            )
        }
    }

    override fun compareTo(other: JigsawEdge): Int {
        this.row.zip(other.row).forEach { (a, b) ->
            if (a != b) return a.compareTo(b)
        }

        return 0
    }
}

data class JigsawTile(val id: Int, val grid: List<List<Boolean>>) {

    val edges = JigsawEdge.fromTile(this)

    override fun toString(): String {
        return "JigsawTile(id=$id)"
    }

    companion object {
        fun parse(text: String): JigsawTile {
            val lines = text.split(sep)
            val header = lines[0]
            val headerMatch = Regex("Tile ([0-9]+):").matchEntire(header) ?: throw IllegalArgumentException()

            val id = headerMatch.groupValues[1].toInt()
            val grid = lines.drop(1)
                .map { row ->
                    row.map {
                        when (it) {
                            '#' -> true
                            '.' -> false
                            else -> throw IllegalArgumentException()
                        }
                    }
                }
            return JigsawTile(id, grid)
        }
    }
}

class Puzzle20(path: String) : Puzzle<List<JigsawTile>, Long>(path) {
    override fun parseInput(reader: BufferedReader) =
        reader.readText().split("$sep$sep")
            .map { JigsawTile.parse(it) }

    override fun solveFirstPart(): Long {
        val edgeMap = input
            .flatMap { tile -> tile.edges.map { it to tile } }
            .groupBy({ (edge, _) -> edge }, { (_, tile) -> tile })

        val data = input.map { tile ->
            tile.edges.count { edge ->
                edgeMap[edge]!!.count() > 1
            }
        }

        val corners = input.filter { tile ->
            tile.edges.count { edge ->
                edgeMap[edge]!!.count() > 1
            } == 2
        }

        println(corners.size)
        println(corners)

        return corners.fold(1) { acc, tile -> acc * tile.id }
    }

    override fun solveSecondPart(): Long {
        TODO()
    }
}

