package y2020

import Puzzle
import java.io.BufferedReader

class Passport(data: Map<String, String>) {

    val birthYear = if (data["byr"] != null) ValidatedInt(data["byr"]!!, 1920, 2002) else Invalid
    val issueYear = if (data["iyr"] != null) ValidatedInt(data["iyr"]!!, 2010, 2020) else Invalid
    val expirationYear = if (data["eyr"] != null) ValidatedInt(data["eyr"]!!, 2020, 2030) else Invalid
    val height = if (data["hgt"] != null) ValidatedHeight(data["hgt"]!!) else Invalid
    val hairColor = if (data["hcl"] != null) ValidatedRegex(data["hcl"]!!, "#[0-9a-f]{6}") else Invalid
    val eyeColor = if (data["ecl"] != null) ValidatedRegex(data["ecl"]!!, "(amb|blu|brn|gry|grn|hzl|oth)") else Invalid
    val passportId = if (data["pid"] != null) ValidatedRegex(data["pid"]!!, "[0-9]{9}") else Invalid
    val countryId = data["cid"]

    fun isPresent(): Boolean {
        return birthYear != Invalid && issueYear != Invalid && expirationYear != Invalid && height != Invalid
                && hairColor != Invalid && eyeColor != Invalid && passportId != Invalid
    }

    fun isValid(): Boolean {
        return birthYear.isValid() && issueYear.isValid() && expirationYear.isValid()
                && height.isValid() && hairColor.isValid() && eyeColor.isValid()
                && passportId.isValid()
    }

    companion object {
        fun parse(text: String): Passport {
            val passportData = text.split(" ", "\n")
                .associate {
                    val (key, value) = it.split(":")
                    key to value
                }

            return Passport(passportData)
        }
    }

    interface Validator {
        fun isValid(): Boolean
    }

    object Invalid : Validator {
        override fun isValid() = false
    }

    class ValidatedInt(data: String, private val min: Int, private val max: Int) : Validator {
        val value = data.toInt()

        override fun isValid(): Boolean {
            return value in min..max
        }
    }

    class ValidatedHeight(data: String) : Validator by when {
        data.endsWith("cm") -> ValidatedInt(data.dropLast(2), 150, 193)
        data.endsWith("in") -> ValidatedInt(data.dropLast(2), 59, 76)
        else -> Invalid
    }

    class ValidatedRegex(val data: String, val pattern: String) : Validator {
        override fun isValid(): Boolean {
            return Regex(pattern).matches(data)
        }
    }
}


class Puzzle4(path: String) : Puzzle<List<Passport>, Int>(path) {
    override fun parseInput(reader: BufferedReader): List<Passport> {
        return reader.readLines()
            .joinToString(separator = "\n")
            .split("\n\n")
            .map { Passport.parse(it) }
    }

    override fun solveFirstPart(): Int {
        return input.count { it.isPresent() }
    }

    override fun solveSecondPart(): Int {
        return input.count { it.isValid() }
    }
}
