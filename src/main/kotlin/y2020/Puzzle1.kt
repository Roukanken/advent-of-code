package y2020

import Puzzle
import SolutionNotFound
import java.io.BufferedReader

class Puzzle1(path: String) : Puzzle<List<Int>, Int>(path) {
    override fun parseInput(reader: BufferedReader) = generateSequence { reader.readLine()?.toInt() }
        .toList()

    private val sorted = input.sorted()

    fun findPairWithSum(target: Int): Pair<Int, Int>? {
        var (start, end) = 0 to sorted.lastIndex

        while (start < end) {
            val sum = sorted[start] + sorted[end]
            when {
                sum < target -> start += 1
                sum == target -> return sorted[start] to sorted[end]
                sum > target -> end -= 1
            }
        }

        return null
    }

    override fun solveFirstPart(): Int {
        val (first, second) = findPairWithSum(2020) ?: throw SolutionNotFound()
        return first * second
    }

    fun findTriadWithSum(target: Int): Triple<Int, Int, Int>? {
        return input.map { it to findPairWithSum(target - it) }
            .filter { it.second != null }
            .map { Triple(it.first, it.second!!.first, it.second!!.second) }
            .firstOrNull()
    }

    override fun solveSecondPart(): Int {
        val (a, b, c) = findTriadWithSum(2020) ?: throw SolutionNotFound()
        return a * b * c
    }

}