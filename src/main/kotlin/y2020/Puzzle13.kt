package y2020

import Puzzle
import SolutionNotFound
import java.io.BufferedReader
import java.math.BigInteger

sealed class BusScheduleEntry {
    data class Bus(val id: Int) : BusScheduleEntry() {
        fun nextDeparture(time: Int) = id - (time % id)
    }

    object Nothing : BusScheduleEntry()

    companion object {
        fun parse(text: String): BusScheduleEntry {
            return if (text == "x") Nothing else Bus(text.toInt())
        }
    }
}

data class BusSchedule(val now: Int, val busses: List<BusScheduleEntry>) {
    companion object {
        fun parse(readLines: List<String>): BusSchedule {
            require(readLines.size == 2) { "invalid input size" }

            val now = readLines[0].trim().toInt()
            val busses = readLines[1]
                .trim()
                .split(",")
                .map { BusScheduleEntry.parse(it) }

            return BusSchedule(now, busses)
        }
    }
}

// Stole this, brain not worki
fun euclidInverse(a: BigInteger, b: BigInteger): BigInteger {
    if (b == 1.toBigInteger()) return 1.toBigInteger()
    var currentA = a
    var currentB = b
    var x0 = 0.toBigInteger()
    var x1 = 1.toBigInteger()
    while (currentA > 1.toBigInteger()) {
        val q = currentA / currentB
        var t = currentB
        currentB = currentA % currentB
        currentA = t
        t = x0
        x0 = x1 - q * x0
        x1 = t
    }
    if (x1 < 0.toBigInteger()) x1 += b
    return x1
}

fun chineseRemainder(modulos: List<BigInteger>, remainders: List<BigInteger>): BigInteger {
    val prod = modulos.fold(1.toBigInteger()) { acc, i -> acc * i }
    var sum = 0.toBigInteger()
    for ((n, a) in modulos.zip(remainders)) {
        val p = prod / n
        sum += a * euclidInverse(p, n) * p
    }
    return sum % prod
}

class Puzzle13(path: String) : Puzzle<BusSchedule, BigInteger>(path) {
    override fun parseInput(reader: BufferedReader) =
        BusSchedule.parse(reader.readLines())

    override fun solveFirstPart(): BigInteger {
        val earliestBus = input.busses
            .filterIsInstance<BusScheduleEntry.Bus>()
            .minByOrNull { it.nextDeparture(input.now) }
            ?: throw SolutionNotFound()

        return earliestBus.nextDeparture(input.now).toBigInteger() * earliestBus.id.toBigInteger()
    }

    override fun solveSecondPart(): BigInteger {
        var remainders = input.busses
            .mapIndexedNotNull { index, entry -> if (entry != BusScheduleEntry.Nothing) -index else null }
            .map { it.toBigInteger() }

        val modulos = input.busses
            .filterIsInstance<BusScheduleEntry.Bus>()
            .map { it.id.toBigInteger() }

        remainders = remainders.zip(modulos).map { (rem, mod) -> rem.mod(mod) }

        assert(remainders.indices == modulos.indices)

        return chineseRemainder(modulos, remainders)
    }
}