package y2020

import Location
import Puzzle
import at
import y2020.Seat.*
import java.io.BufferedReader

enum class Seat(private val char: Char) {
    FLOOR('.'),
    EMPTY('L'),
    FULL('#'),
    ;

    override fun toString(): String {
        return char.toString()
    }

    companion object {
        fun parse(char: Char): Seat = when (char) {
            '.' -> FLOOR
            'L' -> EMPTY
            '#' -> FULL
            else -> throw IllegalArgumentException("'$char' is not a representation of Seat")
        }
    }
}

abstract class AutomataRule<T> {
    abstract operator fun invoke(automata: CellAutomata<T>, location: Location): T
}

object NeighborhoodsRule : AutomataRule<Seat>() {
    override fun invoke(automata: CellAutomata<Seat>, location: Location): Seat {
        val current = automata[location]
        if (current == FLOOR) return FLOOR

        val fullCount = location.neighborhood()
            .mapNotNull { automata.getOrNull(it) }
            .count { it == FULL }

        return when {
            current == EMPTY && fullCount == 0 -> FULL
            current == FULL && fullCount >= 4 -> EMPTY
            else -> current
        }
    }
}

object VisibilityRule : AutomataRule<Seat>() {
    override fun invoke(automata: CellAutomata<Seat>, location: Location): Seat {
        val current = automata[location]
        if (current == FLOOR) return FLOOR

        val visibleFullCount = Location.eightDirections
            .map { direction ->
                location.ray(direction)
                    .map { automata.getOrNull(it) }
                    .firstOrNull { it != FLOOR }
            }
            .count { it == FULL }

        return when {
            current == EMPTY && visibleFullCount == 0 -> FULL
            current == FULL && visibleFullCount >= 5 -> EMPTY
            else -> current
        }
    }
}


data class CellAutomata<T>(val grid: List<List<T>>, val rule: AutomataRule<T>) : Iterable<T> {

    init {
        require(grid.isNotEmpty()) { "Automata is empty" }
        require(grid.all { it.size == grid[0].size }) { "Automata is not a rectangle" }
    }

    operator fun get(location: Location): T = grid[location.y][location.x]
    fun getOrNull(location: Location): T? = grid.getOrNull(location.y)?.getOrNull(location.x)

    fun next(): CellAutomata<T> {
        val newGrid = grid.mapIndexed { y, row ->
            row.mapIndexed { x, _ -> rule(this, x at y) }
        }

        return CellAutomata(newGrid, rule)
    }

    override fun iterator(): Iterator<T> {
        return grid.asIterable()
            .flatten()
            .iterator()
    }

    fun toReadable(): String {
        return grid.joinToString(separator = "\n") {
            it.joinToString(separator = "")
        }
    }

    override fun toString(): String {
        return "CellAutomata {\n${toReadable()}\n}\n"
    }

    companion object {
        fun <T> parse(text: String, rule: AutomataRule<T>, cellParser: (Char) -> T): CellAutomata<T> {
            val grid = text.trim().split("\n")
                .map { it.map(cellParser) }

            return CellAutomata(grid, rule)
        }
    }
}

class Puzzle11(path: String) : Puzzle<CellAutomata<Seat>, Int>(path) {
    override fun parseInput(reader: BufferedReader) =
        CellAutomata.parse(reader.readLines().joinToString("\n"), NeighborhoodsRule, Seat::parse)

    override fun solveFirstPart(): Int {
        val (_, stabilized) = generateSequence(input.copy(rule = NeighborhoodsRule)) { it.next() }
            .windowed(2)
            .takeWhile { (prev, next) -> prev != next }
            .last()

        return stabilized.count { it == FULL }
    }

    override fun solveSecondPart(): Int {
        val (_, stabilized) = generateSequence(input.copy(rule = VisibilityRule)) { it.next() }
            .windowed(2)
            .takeWhile { (prev, next) -> prev != next }
            .last()

        return stabilized.count { it == FULL }
    }
}