package y2020

import Puzzle
import SolutionNotFound
import java.io.BufferedReader

class Puzzle25(path: String) : Puzzle<Pair<Long, Long>, Long>(path) {
    override fun parseInput(reader: BufferedReader): Pair<Long, Long> {
        val (a, b) = reader.readLines()
            .map { it.trim() }
            .filter { it.isNotEmpty() }
            .map { it.toLong() }

        return a to b
    }

    fun findExponent(base: Long, modulo: Long, result: Long): Long {
        var current = 1L

        for (exponent in 1..modulo) {
            current = (current * base) % modulo
            if (current == result) return exponent
        }

        throw SolutionNotFound()
    }

    override fun solveFirstPart(): Long {
        val firstExponent = findExponent(base, modulo, input.first)

        var result = 1L

        for (exponent in 1..firstExponent) {
            result = (result * input.second) % modulo
        }

        return result
    }

    override fun solveSecondPart(): Long {
        TODO()
    }

    companion object {
        const val base = 7L
        const val modulo = 20201227L
    }
}

