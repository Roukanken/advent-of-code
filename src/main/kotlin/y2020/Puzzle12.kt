package y2020

import Location
import Puzzle
import at
import java.io.BufferedReader
import kotlin.math.abs

data class Ship(val location: Location, val direction: Int, val waypoint: Location = 10 at 1) {
    fun move(delta: Location): Ship = copy(location = location + delta)
    fun moveWaypoint(delta: Location): Ship = copy(waypoint = waypoint + delta)
}

fun rotateRight(loc: Location) = Location(loc.y, -loc.x)

sealed class ShipInstruction {
    abstract fun execute(ship: Ship): Ship

    data class North(val amount: Int) : ShipInstruction() {
        override fun execute(ship: Ship): Ship = ship.move(0 at amount)
    }

    data class South(val amount: Int) : ShipInstruction() {
        override fun execute(ship: Ship): Ship = ship.move(0 at -amount)
    }

    data class East(val amount: Int) : ShipInstruction() {
        override fun execute(ship: Ship): Ship = ship.move(amount at 0)
    }

    data class West(val amount: Int) : ShipInstruction() {
        override fun execute(ship: Ship): Ship = ship.move(-amount at 0)
    }

    data class Left(val amount: Int) : ShipInstruction() {
        override fun execute(ship: Ship): Ship = ship.copy(direction = Math.floorMod(ship.direction - amount / 90, 4))
    }

    data class Right(val amount: Int) : ShipInstruction() {
        override fun execute(ship: Ship): Ship = ship.copy(direction = Math.floorMod(ship.direction + amount / 90, 4))
    }

    data class Forward(val amount: Int) : ShipInstruction() {
        override fun execute(ship: Ship): Ship {
            val direction = Location.fourDirections[ship.direction]
            return ship.move(direction * amount)
        }
    }

    companion object {
        fun parse(text: String): ShipInstruction {
            val amount = text.drop(1).toInt()

            return when (text[0]) {
                'N' -> North(amount)
                'S' -> South(amount)
                'E' -> East(amount)
                'W' -> West(amount)
                'L' -> Left(amount)
                'R' -> Right(amount)
                'F' -> Forward(amount)
                else -> throw IllegalArgumentException("Unknown type of instruction '${text[0]}'")
            }
        }
    }
}

sealed class WaypointInstruction {
    abstract fun execute(ship: Ship): Ship

    data class North(val amount: Int) : WaypointInstruction() {
        override fun execute(ship: Ship): Ship = ship.moveWaypoint(0 at amount)
    }

    data class South(val amount: Int) : WaypointInstruction() {
        override fun execute(ship: Ship): Ship = ship.moveWaypoint(0 at -amount)
    }

    data class East(val amount: Int) : WaypointInstruction() {
        override fun execute(ship: Ship): Ship = ship.moveWaypoint(amount at 0)
    }

    data class West(val amount: Int) : WaypointInstruction() {
        override fun execute(ship: Ship): Ship = ship.moveWaypoint(-amount at 0)
    }

    data class Left(val amount: Int) : WaypointInstruction() {
        override fun execute(ship: Ship): Ship {
            var result = ship

            repeat(Math.floorMod(-amount / 90, 4)) {
                result = result.copy(waypoint = rotateRight(result.waypoint))
            }

            return result
        }
    }

    data class Right(val amount: Int) : WaypointInstruction() {
        override fun execute(ship: Ship): Ship {
            var result = ship

            repeat(Math.floorMod(amount / 90, 4)) {
                result = result.copy(waypoint = rotateRight(result.waypoint))
            }

            return result
        }
    }

    data class Forward(val amount: Int) : WaypointInstruction() {
        override fun execute(ship: Ship): Ship = ship.move(ship.waypoint * amount)
    }

    companion object {
        fun parse(text: String): WaypointInstruction {
            val amount = text.drop(1).toInt()

            return when (text[0]) {
                'N' -> North(amount)
                'S' -> South(amount)
                'E' -> East(amount)
                'W' -> West(amount)
                'L' -> Left(amount)
                'R' -> Right(amount)
                'F' -> Forward(amount)
                else -> throw IllegalArgumentException("Unknown type of instruction '${text[0]}'")
            }
        }
    }
}


class Puzzle12(path: String) : Puzzle<List<String>, Int>(path) {
    override fun parseInput(reader: BufferedReader) =
        reader.readLines()
            .filter { it.isNotBlank() }

    override fun solveFirstPart(): Int {
        val initialShip = Ship(0 at 0, 0)
        val finalShip = input
            .map { ShipInstruction.parse(it.trim()) }
            .fold(initialShip) { ship, instruction -> instruction.execute(ship) }

        return abs(finalShip.location.x) + abs(finalShip.location.y)
    }

    override fun solveSecondPart(): Int {
        val initialShip = Ship(0 at 0, 0)
        val finalShip = input
            .map { WaypointInstruction.parse(it.trim()) }
            .fold(initialShip) { ship, instruction -> instruction.execute(ship) }

        return abs(finalShip.location.x) + abs(finalShip.location.y)
    }
}