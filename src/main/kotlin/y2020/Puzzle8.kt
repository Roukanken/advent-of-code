package y2020

import Puzzle
import SolutionNotFound
import computeReachable
import y2020.Instruction.*
import java.io.BufferedReader

interface InstructionParser {
    fun parse(arguments: List<String>): Instruction
}

sealed class Instruction {

    abstract fun execute(state: Console): Console

    data class NoOperation(val value: Int) : Instruction() {
        override fun execute(state: Console) = state.nextInstruction()

        companion object : InstructionParser {
            override fun parse(arguments: List<String>): Instruction {
                return NoOperation(arguments.first().toInt())
            }
        }
    }

    data class Accumulate(val value: Int) : Instruction() {
        override fun execute(state: Console) = state.copy(accumulator = state.accumulator + value).nextInstruction()

        companion object : InstructionParser {
            override fun parse(arguments: List<String>): Instruction {
                return Accumulate(arguments.first().toInt())
            }
        }
    }

    data class Jump(val value: Int) : Instruction() {
        override fun execute(state: Console) = state.copy(instructionPointer = state.instructionPointer + value)

        companion object : InstructionParser {
            override fun parse(arguments: List<String>): Instruction {
                return Jump(arguments.first().toInt())
            }
        }
    }

    companion object {
        private val MAPPING = mapOf(
            "nop" to NoOperation,
            "acc" to Accumulate,
            "jmp" to Jump,
        )

        fun parse(text: String): Instruction {
            val tokens = text.trim().split(" ")
            val instructionName = tokens.first()

            return MAPPING[instructionName]?.parse(tokens.drop(1))
                ?: throw IllegalArgumentException("Found unknown instruction $instructionName")
        }
    }
}

data class Console(
    val accumulator: Int = 0,
    val instructionPointer: Int = 0,
) {

    fun nextInstruction(): Console {
        return this.copy(instructionPointer = instructionPointer + 1)
    }

    fun execute(instructions: List<Instruction>) =
        generateSequence(this) { instructions[it.instructionPointer].execute(it) }
            .takeWhile { it.instructionPointer != instructions.size }

    companion object {
        fun execute(instructions: List<Instruction>) = Console().execute(instructions)
    }
}

class Puzzle8(path: String) : Puzzle<List<Instruction>, Int>(path) {
    override fun parseInput(reader: BufferedReader): List<Instruction> {
        return reader.readLines()
            .filter { it.isNotBlank() }
            .map { Instruction.parse(it) }
    }

    override fun solveFirstPart(): Int {
        val executed = mutableSetOf<Int>()

        for (state in Console.execute(input)) {
            if (state.instructionPointer in executed) {
                return state.accumulator
            }

            executed.add(state.instructionPointer)
        }

        throw SolutionNotFound()
    }

    override fun solveSecondPart(): Int {
        fun modifyInstruction(instruction: Instruction) = when (instruction) {
            is Jump -> NoOperation(instruction.value)
            is NoOperation -> Jump(instruction.value)
            is Accumulate -> null
        }

        val executionMap = input.mapIndexed { index, instruction ->
            val console = instruction.execute(Console(0, index))
            index to listOf(console.instructionPointer)
        }.toMap()

        val reachableFromStart = executionMap.computeReachable(0)
        val reverseMap = executionMap.reverseNeighborhoodMap()
        val executesToEnd = reverseMap.computeReachable(input.size)

        val (winningInstruction, _) = reachableFromStart.mapNotNull { input.getOrNull(it)?.to(it) }
            .mapNotNull { (instruction, index) -> modifyInstruction(instruction)?.to(index) }
            .map { (instruction, index) ->
                index to instruction.execute(Console(instructionPointer = index)).instructionPointer
            }
            .single { (_, target) -> target in executesToEnd }

        val modifiedInput = input.toMutableList()
        modifiedInput[winningInstruction] = modifyInstruction(modifiedInput[winningInstruction])!!

        return Console.execute(modifiedInput).last().accumulator
    }
}

fun <T> Map<T, List<T>>.reverseNeighborhoodMap() =
    this.toList()
        .flatMap { (key, neighborhoods) -> neighborhoods.map { key to it } }
        .groupBy { it.second }
        .mapValues { (_, value) -> value.map { it.first } }