import java.io.BufferedReader

abstract class Puzzle<I, O>(resourcePath: String) : ComplicatedPuzzle<I, O, O>(resourcePath)

abstract class ComplicatedPuzzle<I, O1, O2>(private val resourcePath: String) {
    open val input = readInput()

    abstract fun parseInput(reader: BufferedReader): I
    abstract fun solveFirstPart(): O1
    abstract fun solveSecondPart(): O2

    private fun readInput(): I {
        val input = this::class.java.getResource(resourcePath).readText()
        return parseInput(input.reader().buffered())
    }
}
