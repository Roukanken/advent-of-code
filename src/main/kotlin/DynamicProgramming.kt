import java.util.*

data class Dynamic<State, Value, Category>(
    val start: Iterable<Pair<State, Value>>,
    val categorize: (State) -> Category,
    val comparator: Comparator<Category>,
    val nextStates: (State) -> Iterable<State>,
    val fold: Fold<Value>,
    val filter: (State) -> Boolean = { true },
) {

    fun compute(): Map<State, Value> {
        val result = start.toMap().toMutableMap().withDefault { fold.default }

        val work = result.keys
            .groupBy { categorize(it) }
            .mapValues { it.value.toMutableSet() }
            .toMutableMap()
            .withDefault { mutableSetOf() }

        val categoryQueue = PriorityQueue(comparator).apply {
            addAll(work.keys)
        }

        while (categoryQueue.isNotEmpty()) {
            val currentCategory = categoryQueue.remove()

            for (state in work.getValue(currentCategory)) {
                val currentValue = result.getValue(state)

                nextStates(state)
                    .filter(filter)
                    .forEach { nextState ->
                        result[nextState] = fold.reduce(currentValue, result.getValue(nextState))

                        val nextCategory = categorize(nextState)
                        check(comparator.compare(currentCategory, nextCategory) < 0) {
                            """
                                categorize(state) < categorize(nextState) must be true
                                current: $state with category $currentCategory
                                next: $nextState with category $nextCategory
                            """.trimIndent()
                        }

                        work.getOrPut(nextCategory) {
                            categoryQueue.add(nextCategory)
                            mutableSetOf()
                        }.add(nextState)
                    }
            }
        }

        return result
    }

    data class Fold<Value>(val default: Value, val reduce: (Value, Value) -> Value)
}

fun <Value> fold(default: Value, reduce: (Value, Value) -> Value) = Dynamic.Fold(default, reduce)

inline fun <T> ((T) -> Boolean).and(crossinline predicate: (T) -> Boolean): (T) -> Boolean = { this(it) && predicate(it) }
